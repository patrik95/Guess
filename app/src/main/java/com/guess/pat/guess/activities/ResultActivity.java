package com.guess.pat.guess.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.guess.pat.guess.R;
import com.guess.pat.guess.models.GameResult;
import com.guess.pat.guess.models.HistoryRecord;
import com.guess.pat.guess.models.NotificationModel;
import com.guess.pat.guess.models.ResponseModel;
import com.guess.pat.guess.models.SavedRecords;
import com.guess.pat.guess.models.SendModel;
import com.guess.pat.guess.models.SpinnerItem;
import com.guess.pat.guess.models.UserProfile;
import com.guess.pat.guess.services.APIService;
import com.guess.pat.guess.services.Common;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Paťo on 25.2.2018.
 */

public class ResultActivity extends AppCompatActivity {

    // uid of guessed user
    private String guessed_user_uid;
    private String mDeviceToken;
    APIService mApiService;

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mProfileDatabaseReference;
    private StorageReference mVoiceStorageReference;

    private GameResult mGameResult;
    private TextView mResultText;
    private TextView mResultScore;

    // progress bar layouts
    private RelativeLayout mProgressBarLayout;
    private RelativeLayout mButtonsLayout;

    private long mLastClickTime = 0;
    ArrayList<SpinnerItem> spinnerItems = new ArrayList<>();
    @Override
    public void onCreate(Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mProfileDatabaseReference = mFirebaseDatabase.getReference().child("profiles");
        mVoiceStorageReference = FirebaseStorage.getInstance().getReference("user_voices");

        // getting result from game
        mGameResult = (GameResult) getIntent().getSerializableExtra("RESULT");
        guessed_user_uid = getIntent().getStringExtra("GUESSEDUID");
        mDeviceToken = getIntent().getStringExtra("DEVICETOKEN");

        DateFormat df = new SimpleDateFormat("dd.MM. yyyy");
        String date = df.format(Calendar.getInstance().getTime());
        mGameResult.setDate(date);

        mResultText = findViewById(R.id.id_result_text);
        mResultText.setText(mGameResult.getMessage().getText());

        mResultScore = findViewById(R.id.id_result_score);
        mResultScore.setText(mGameResult.getResult() + "%");

        // progress bar
        mProgressBarLayout = findViewById(R.id.id_progress_bar_layout);
        mButtonsLayout = findViewById(R.id.id_buttons_layout);

        Button buttonMenu = findViewById(R.id.id_menu);
        buttonMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // missclick and multiclick prevention by time checking
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                saveGameResult();

                // open new activity (MainActivity)
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        Button buttonDetail = findViewById(R.id.id_detail);
        buttonDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // missclick and multiclick prevention by time checking
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                intent.putExtra("GAME", mGameResult);
                startActivity(intent);
            }
        });

        Button buttonNewGame = findViewById(R.id.id_new_game);
        buttonNewGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // missclick and multiclick prevention by time checking
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                saveGameResult();

                // find player's profile
                mProfileDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // show progress bar and gray down background
                        mProgressBarLayout.setVisibility(View.VISIBLE);
                        mButtonsLayout.setAlpha((float) 0.5);
                        mButtonsLayout.setClickable(false);

                        getUserProfile(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {}
                });
            }
        });
    }

    private void saveGameResult() {
        Context context = getApplicationContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                getString(R.string.user_records), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        SavedRecords savedRecords = new SavedRecords();
        String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String id = "result_" + userUid; // get storage key
        String result_json = sharedPreferences.getString(id,null);

        // check if there are any saved records
        if (result_json == null ) {
            // save records
            savedRecords.getGameRecords().add(0, mGameResult);

            // transfer  object to json
            result_json = gson.toJson(savedRecords);
            editor.putString(id, result_json);
            editor.commit();
        } else {
            savedRecords = gson.fromJson(result_json, SavedRecords.class);

            // save records
            savedRecords.getGameRecords().add(0, mGameResult);

            // transfer  object to json
            result_json = gson.toJson(savedRecords);
            editor.putString(id, result_json);
            editor.commit();
        }
        // update database of user who was playing game (guessing)
        DatabaseReference reference = mProfileDatabaseReference.child(userUid).child("records").push();
        reference.setValue(mGameResult);

        // update database of user who's profile was guessed
        reference = mProfileDatabaseReference.child(guessed_user_uid).child("history").push();
        HistoryRecord historyRecord = new HistoryRecord(mGameResult.getResult(), mGameResult.getPartResults(), mGameResult.getDate());
        reference.setValue(historyRecord);

        mApiService = Common.getFCMClient();
        // send notification
        NotificationModel notificationModel = new NotificationModel("Check out your history.","Your profile was guessed!");
        SendModel sendModel = new SendModel(mDeviceToken, notificationModel);
        mApiService.sendNotification(sendModel)
                .enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                        // success
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
                        // fail
                    }
                });


    }

    private void getUserProfile(DataSnapshot dataSnapshot){
        // update time to block multiple clicks
        mLastClickTime = SystemClock.elapsedRealtime();

        long numChildren = dataSnapshot.getChildrenCount();
        // generate random user id
        Random randomGenerator = new Random();
        int userIndex =  randomGenerator.nextInt((int) numChildren);

        // user profile which will be selected
        UserProfile profile = null;
        String userID = null;

        Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
        int length = (int) dataSnapshot.getChildrenCount();

        // go trought all users and pick random selected user
        for(int i = 0; i < length; i++) {
            if(i == userIndex) {
                profile = iterator.next().getValue(UserProfile.class);
                if(profile.getVoiceUrls() == null || profile.getAge().equals("")
                        || profile.getEducation().equals("") || profile.getCountry().equals("")) {
                    getUserProfile(dataSnapshot);
                    break;
                } else {
                    int startIndex = 0;
                    userID = getUid(i, dataSnapshot);
                    String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                    // user can't guess his own profile
                    if (userID.equals(userUid)){
                        getUserProfile(dataSnapshot);
                    } else {
                        downloadRecordings(startIndex, userID, profile);
                    }
                    break;
                }

            } else {
                iterator.next();
            }
        }
    }

    private String getUid(int index, DataSnapshot dataSnapshot){
        int i=0;
        Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
        for(;;) {
            if(i == index){
                return iterator.next().getKey();
            } else {
                iterator.next();
                i++;
            }
        }
    }

    private void downloadRecordings(int i, final String userUid, final UserProfile profile){
        final int index = i;

        // get filename from string path
        int lastIndex = profile.getVoiceUrls().get(index).indexOf("alt=") - 1;
        int startIndex = profile.getVoiceUrls().get(index).indexOf("Voice_");
        final String fileName  = profile.getVoiceUrls().get(index).substring(startIndex , lastIndex);

        StorageReference filepath = mVoiceStorageReference.child(userUid).child(fileName);

        File rootPath = new File(new File(getExternalFilesDir("user_records"), "tmp_downloaded_records"),profile.getUsername());
        rootPath.mkdirs();
        final File localFile = new File(rootPath,fileName+".3gp");

        filepath.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                // add files to array list for spinner
                spinnerItems.add(new SpinnerItem(index, fileName, Uri.fromFile(localFile).toString()));
                int nextIndex = index + 1;
                if (nextIndex >= profile.getVoiceUrls().size()) {
                    profile.setSpinnerItems(spinnerItems);
                    // open new activity (PlayActivity)
                    Intent intent = new Intent(getBaseContext(), PlayActivity.class);
                    intent.putExtra("PROFILE", profile);
                    intent.putExtra("GUESSEDUID", userUid);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

                    mProgressBarLayout.setVisibility(View.INVISIBLE);
                    mButtonsLayout.setClickable(true);
                    mButtonsLayout.setAlpha(1);

                    startActivity(intent);
                    finish();
                } else {
                    downloadRecordings(nextIndex, userUid, profile);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });
    }
}
