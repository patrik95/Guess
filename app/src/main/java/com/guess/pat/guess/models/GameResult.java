package com.guess.pat.guess.models;


import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Paťo on 23.2.2018.
 */

public class GameResult implements Serializable {

    private String date;
    private String result;
    private int resultNumber;
    private Messages message;
    private ArrayList<PartResult> partResults = new ArrayList<>();


    public GameResult() {

    }

    public GameResult(int resultNumber, Messages message, ArrayList<PartResult> partResults) {
        this.resultNumber = resultNumber;
        this.message = message;
        this.partResults = partResults;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    // TODO: calculate it if possible
    public String getResult() {
        switch (this.resultNumber){
            case 0:
                return "100";
            case -1:
                return "0";
            case 1:
                return "20";
            case 2:
                return "40";
            case 3:
                return "60";
            case 4 :
                return "80";
            case 5:
                return "100";
            default:
                return "0";
        }
    }

    public void setResultNumber(int resultNumber) {
        this.resultNumber = resultNumber;
        if ( resultNumber == Messages.values().length-2) {
            this.setMessage(Messages.valueOf(0));
        } else {
            this.setMessage(Messages.valueOf(resultNumber));
        }
        this.result = getResult();
    }

    public Messages getMessage() {
        return message;
    }

    public void setMessage(Messages message) {
        this.message = message;
    }

    public ArrayList<PartResult> getPartResults() {
        return partResults;
    }

    public void setPartResults(ArrayList<PartResult> partResults) {
        this.partResults = partResults;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getResultNumber() {
        return resultNumber;
    }
}
