package com.guess.pat.guess.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.guess.pat.guess.fragments.RecordsFragment;

/**
 * Created by Paťo on 25.2.2018.
 */

public class FragmentTabsAdapter extends FragmentPagerAdapter {

    final int TABS_COUNT = 2;
    private String tabTitles[] = new String[]{"My plays", "Results"};
    private Context context;

    public FragmentTabsAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        return RecordsFragment.newInstance(position+1);
    }

    @Override
    public int getCount() {
        return TABS_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
