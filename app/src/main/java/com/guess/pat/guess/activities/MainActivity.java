package com.guess.pat.guess.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.guess.pat.guess.R;
import com.guess.pat.guess.models.ConfigurationModel;
import com.guess.pat.guess.models.GameResult;
import com.guess.pat.guess.models.HistoryRecord;
import com.guess.pat.guess.models.SavedRecords;
import com.guess.pat.guess.models.SpinnerItem;
import com.guess.pat.guess.models.UserProfile;
import com.guess.pat.guess.services.Common;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    public static final String ANONYMOUS = "anonymous";

    public static final int RC_SIGN_IN = 1;

    // Firebase instance variables
    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private DatabaseReference mFirebaseDatabase;
    private StorageReference mVoiceStorageReference;

    private String mUsername;
    ArrayList<String> mVoices = new ArrayList<>();

    ArrayList<SpinnerItem> spinnerItems = new ArrayList<>();

    // progress bar layouts
    private RelativeLayout mProgressBarLayout;
    private RelativeLayout mButtonsLayout;

    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // check if is internet connection
        Common common = new Common();
        common.isDeviceConnected(this);

        mUsername = ANONYMOUS;

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference("profiles");
        mVoiceStorageReference = FirebaseStorage.getInstance().getReference("user_voices");

        mProgressBarLayout = findViewById(R.id.id_progress_bar_layout);
        mButtonsLayout = findViewById(R.id.id_buttons_layout);

        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    //the user is signed in
                    onSignedInInitialize(user.getDisplayName());
                } else {
                    //the user is signed out
                    onSignedOutCleanup();
                    startActivityForResult(
                            AuthUI.getInstance()
                                    .createSignInIntentBuilder()
                                    .setIsSmartLockEnabled(false)
                                    .setPrivacyPolicyUrl("https://sites.google.com/view/guesswho")
                                    .setAvailableProviders(
                                            Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                                                    new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build()))
                                    .setTheme(R.style.FirebaseTheme)
                                    .build(),
                            RC_SIGN_IN);
                }
            }
        };

        final Button button = findViewById(R.id.id_profile);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // missclick and multiclick prevention by time checking
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                // show progress bar and gray down background
                TextView textView = findViewById(R.id.id_progress_bar_textView);
                textView.setText("Loading...");
                mProgressBarLayout.setVisibility(View.VISIBLE);
                mButtonsLayout.setAlpha((float) 0.5);
                mButtonsLayout.setClickable(false);
                // find player's profile
                FirebaseDatabase.getInstance().getReference("configuration").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
                        int length = (int) dataSnapshot.getChildrenCount();

                        // go through all config options
                        for(int i = 0; i < length; i++) {
                            // get the wanted config
                            if (i == 0) {
                                ConfigurationModel configModel = iterator.next().getValue(ConfigurationModel.class);

                                final Context context = getApplicationContext();
                                SharedPreferences sharedPreferences = context.getSharedPreferences(
                                        getString(R.string.configuration), Context.MODE_PRIVATE);

                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                Gson gson = new Gson();
                                String userUid = FirebaseAuth.getInstance().getUid();
                                String id = "configuration_" + userUid; // get storage key

                                // save configuration
                                String configuration_json = gson.toJson(configModel);
                                editor.putString(id, configuration_json);
                                editor.commit();
                            }
                        }
                        // check if user exists in database and in device
                        firstTimeProfileCheck();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {}
                });
            }
        });

        Button playButton = findViewById(R.id.id_play);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // missclick and multiclick prevention by time checking
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                // find player's profile
                mFirebaseDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // show progress bar and gray down background
                        mProgressBarLayout.setVisibility(View.VISIBLE);
                        mButtonsLayout.setAlpha((float) 0.5);
                        mButtonsLayout.setClickable(false);

                        getUserProfile(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {}
                });
            }
        });

        Button recordButton = findViewById(R.id.id_record);
        recordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // missclick and multiclick prevention by time checking
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                // set progressbar
                TextView textView = findViewById(R.id.id_progress_bar_textView);
                textView.setText("Loading...");
                mProgressBarLayout.setVisibility(View.VISIBLE);
                mButtonsLayout.setAlpha((float) 0.5);
                mButtonsLayout.setClickable(false);

                downloadHistory();

            }
        });

        Button buttonQuit = findViewById(R.id.id_quit);
        buttonQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Button aboutButton = findViewById(R.id.id_about);
        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), AboutActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "Signed in!", Toast.LENGTH_SHORT).show();

            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Sign in canceled.", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    // Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.sign_out_menu:
                //sign out
                AuthUI.getInstance().signOut(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Authorization
    @Override
    protected void onPause() {
        super.onPause();
        mFirebaseAuth.removeAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }

    private void onSignedInInitialize(String username) {
        mUsername = username;
    }

    private void onSignedOutCleanup() {
        mUsername = ANONYMOUS;
    }

    // check if user have profile in database, but not in device (user logged to new device)
    private void firstTimeProfileCheck() {
        final String userUID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        // check if user exists in database
        mFirebaseDatabase.child(userUID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    Gson gson = new Gson();
                    Context context = getApplicationContext();
                    SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.user_profile), Context.MODE_PRIVATE);

                    String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                    String id = "profile_" + userUid; // get storage key

                    String user_json = sharedPref.getString(id,"");
                    UserProfile userProfile  = gson.fromJson(user_json, UserProfile.class);

                    // if doesn't exist in device, download profile
                    if (userProfile == null) {
                        userProfile = snapshot.getValue(UserProfile.class);
                        if (!(userProfile.getVoiceUrls() == null)) {
                            downloadRecordingsForProfile(0, userUID, userProfile);
                        } else {
                            storeUserData(userProfile);
                        }


                    } else {
                        startProfileActivity();
                    }

                } else {
                    startProfileActivity();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    private void downloadRecordingsForProfile(int i, final String userUid, final UserProfile profile){
        final int index = i;


        // get filename from string path
        int lastIndex = 0;
        int startIndex = 0;
        lastIndex = profile.getVoiceUrls().get(index).indexOf("alt=") - 1;
        startIndex = profile.getVoiceUrls().get(index).indexOf("Voice_");
        String tmpFile = profile.getVoiceUrls().get(index).substring(startIndex , lastIndex);;
        // check for manually added files
        if ((profile.getVoiceUrls().get(index).indexOf(".3gp") != -1)) {
            lastIndex = profile.getVoiceUrls().get(index).indexOf("alt=") - 5;
            startIndex = profile.getVoiceUrls().get(index).indexOf("Voice_");
        }
        final String fileName  = profile.getVoiceUrls().get(index).substring(startIndex , lastIndex);

        StorageReference filepath = mVoiceStorageReference.child(userUid).child(tmpFile);
        final File rootPath = new File(getExternalFilesDir("user_records"), profile.getUsername());
        rootPath.mkdirs();
        File localFile = null;
        if(fileName.indexOf(".3gp") == -1){
            localFile = new File(rootPath,fileName + ".3gp");
        } else {
            localFile = new File(rootPath, fileName);
        }

        filepath.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                // add files to array list for spinner
                File localFile = null;
                if(fileName.indexOf(".3gp") == -1){
                    localFile = new File(rootPath,fileName + ".3gp");
                } else {
                    localFile = new File(rootPath, fileName);
                }
                spinnerItems.add(new SpinnerItem(index, fileName, Uri.fromFile(localFile).toString()));
                int nextIndex = index + 1;
                if (nextIndex >= profile.getVoiceUrls().size()) {
                    profile.setSpinnerItems(spinnerItems);
                    storeUserData(profile);

                } else {
                    downloadRecordingsForProfile(nextIndex, userUid, profile);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });
    }

    private void storeUserData(UserProfile profile) {
        // convert User object user to JSON format
        Gson gson = new Gson();
        String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String user_json = gson.toJson(profile);

        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.user_profile), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        // store in SharedPreferences
        String id = "profile_" + userUid; // get storage key

        editor.putString(id, user_json);
        editor.commit();

        startProfileActivity();
    }

    private void startProfileActivity(){
        UserProfile profile = new UserProfile();
        profile.setUsername(mUsername);
        profile.setVoiceUrls(mVoices);

        // open new activity (ProfileActivity)
        Intent intent = new Intent(getBaseContext(), ProfileActivity.class);
        intent.putExtra("PROFILE", profile);
        intent.putExtra("USERNAME", mUsername);

        mProgressBarLayout.setVisibility(View.INVISIBLE);
        mButtonsLayout.setClickable(true);
        mButtonsLayout.setAlpha(1);

        startActivity(intent);
    }

    private void getUserProfile(DataSnapshot dataSnapshot){
        // update time to block multiple clicks
        mLastClickTime = SystemClock.elapsedRealtime();

        long numChildren = dataSnapshot.getChildrenCount();
        // generate random user id
        Random randomGenerator = new Random();
        int userIndex =  randomGenerator.nextInt((int) numChildren);

        // user profile which will be selected
        UserProfile profile = null;
        String userID = null;

        Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
        int length = (int) dataSnapshot.getChildrenCount();

        // go through all users and pick random selected user
        for(int i = 0; i < length; i++) {
            if(i == userIndex) {
                profile = iterator.next().getValue(UserProfile.class);
                if (!(profile == null) && !(profile.getAge() == null) && !(profile.getEducation() == null)
                        && !(profile.getCountry() == null)){
                    if(profile.getVoiceUrls() == null || profile.getAge().equals("")
                            || profile.getEducation().equals("") || profile.getCountry().equals("")) {
                        getUserProfile(dataSnapshot);
                        break;
                    } else {
                        int startIndex = 0;
                        userID = getUid(i, dataSnapshot); // user to guess
                        String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                        // user can't guess his own profile
                        if (userID.equals(userUid)){
                            getUserProfile(dataSnapshot);
                        } else {
                            downloadRecordings(startIndex, userID, profile);
                        }
                        break;
                    }
                } else {
                    getUserProfile(dataSnapshot);
                }

            } else {
                iterator.next();
            }
        }
    }

    private String getUid(int index, DataSnapshot dataSnapshot){
        int i=0;
        Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
        for(;;) {
            if(i == index){
                return iterator.next().getKey();
            } else {
                iterator.next();
                i++;
            }
        }
    }

    private void downloadRecordings(int i, final String userUid, final UserProfile profile){
        final int index = i;

        // get filename from string path
        int lastIndex = profile.getVoiceUrls().get(index).indexOf("alt=") - 1;
        int startIndex = profile.getVoiceUrls().get(index).indexOf("Voice_");
        final String fileName  = profile.getVoiceUrls().get(index).substring(startIndex , lastIndex);

        StorageReference filepath = mVoiceStorageReference.child(userUid).child(fileName);
        File rootPath = new File(new File(getExternalFilesDir("user_records"), "tmp_downloaded_records"), profile.getUsername());
        rootPath.mkdirs();
        final File localFile = new File(rootPath,fileName+".3gp");

        filepath.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                // add files to array list for spinner
                spinnerItems.add(new SpinnerItem(index, fileName, Uri.fromFile(localFile).toString()));
                int nextIndex = index + 1;
                if (nextIndex >= profile.getVoiceUrls().size()) {
                    profile.setSpinnerItems(spinnerItems);

                    // get config file
                    FirebaseDatabase.getInstance().getReference("configuration").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
                            int length = (int) dataSnapshot.getChildrenCount();

                            // go through all config options
                            for(int i = 0; i < length; i++) {
                                // get the wanted config
                                if (i == 0) {
                                    ConfigurationModel configModel = iterator.next().getValue(ConfigurationModel.class);

                                    final Context context = getApplicationContext();
                                    SharedPreferences sharedPreferences = context.getSharedPreferences(
                                            getString(R.string.configuration), Context.MODE_PRIVATE);

                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    Gson gson = new Gson();
                                    String userUid = FirebaseAuth.getInstance().getUid();
                                    String id = "configuration_" + userUid; // get storage key

                                    // save configuration
                                    String configuration_json = gson.toJson(configModel);
                                    editor.putString(id, configuration_json);
                                    editor.commit();
                                }
                            }
                            // open new activity (PlayActivity)
                            Intent intent = new Intent(getBaseContext(), PlayActivity.class);
                            intent.putExtra("PROFILE", profile);
                            intent.putExtra("GUESSEDUID", userUid);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

                            mProgressBarLayout.setVisibility(View.INVISIBLE);
                            mButtonsLayout.setClickable(true);
                            mButtonsLayout.setAlpha(1);

                            startActivity(intent);
                            finish();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {}
                    });

                } else {
                    downloadRecordings(nextIndex, userUid, profile);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });
    }

    private void downloadHistory(){
        String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference historyReference = FirebaseDatabase.getInstance().getReference().child("profiles").child(userUid).child("history");
        historyReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // history items iterator
                Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
                int length = (int) dataSnapshot.getChildrenCount();
                SavedRecords savedRecords = new SavedRecords();

                // add all history items to array
                ArrayList<HistoryRecord> historyRecords = new ArrayList<>();
                for (int i = 0; i < length; i++) {
                    historyRecords.add(iterator.next().getValue(HistoryRecord.class));
                }
                // save array for further use
                savedRecords.setHistory(historyRecords);
                downloadRecords(savedRecords);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void downloadRecords(SavedRecords savedRecords){
        final SavedRecords tmpSavedRecords = savedRecords;
        String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        final DatabaseReference historyReference = FirebaseDatabase.getInstance().getReference().child("profiles").child(userUid).child("records");
        historyReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // history items iterator
                Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
                int length = (int) dataSnapshot.getChildrenCount();

                Context context = getApplicationContext();
                SharedPreferences sharedPreferences = context.getSharedPreferences(
                        getString(R.string.user_records), Context.MODE_PRIVATE);

                SharedPreferences.Editor editor = sharedPreferences.edit();
                Gson gson = new Gson();
                SavedRecords savedRecords = tmpSavedRecords;
                String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                String id = "downloaded_records_" + userUid; // get storage key

                // add all history items to array
                ArrayList<GameResult> gameResults = new ArrayList<>();
                int count = 0;
                for (int i = 0; i < length; i++) {
                    GameResult gameResult = new GameResult();
                    HistoryRecord tmpHistory = iterator.next().getValue(HistoryRecord.class);
                    gameResult.setDate(tmpHistory.getDate());
                    gameResult.setPartResults(tmpHistory.getPartResults());
                    // count partial results
                    for (int j = 0; j < tmpHistory.getPartResults().size(); j++) {
                        if(tmpHistory.getPartResults().get(j).getPartResult()){
                            count++;
                            gameResult.setResultNumber(count);
                        } else {
                            if(count == 0){
                                gameResult.setResultNumber(-1);
                            }
                        }
                    }
                    gameResults.add(gameResult);
                    count = 0;
                }
                // save array for further use
                savedRecords.setGameRecords(gameResults);
                String downloaded_records_json = gson.toJson(savedRecords);
                editor.putString(id, downloaded_records_json);
                editor.commit();
                // start new activity
                Intent intent = new Intent(getBaseContext(), RecordsActivity.class);
                // progress bar off
                mProgressBarLayout.setVisibility(View.INVISIBLE);
                mButtonsLayout.setClickable(true);
                mButtonsLayout.setAlpha(1);
                startActivity(intent);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
