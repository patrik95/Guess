package com.guess.pat.guess.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.gson.Gson;
import com.guess.pat.guess.R;
import com.guess.pat.guess.activities.DetailActivity;
import com.guess.pat.guess.adapters.FragmentAdapter;
import com.guess.pat.guess.models.FragmentModel;
import com.guess.pat.guess.models.GameResult;
import com.guess.pat.guess.models.HistoryRecord;
import com.guess.pat.guess.models.SavedRecords;

import java.util.ArrayList;

/**
 * Created by Paťo on 25.2.2018.
 */

public class RecordsFragment extends Fragment {

    ArrayList<FragmentModel> fragmentModelsForRecords = new ArrayList<>();
    ArrayList<FragmentModel> fragmentModelsForHistory = new ArrayList<>();
    ListView listView;
    private DatabaseReference historyReference;
    private static FragmentAdapter mFragmentAdapterTab1;
    private static FragmentAdapter mFragmentAdapterTab2;
    private SavedRecords mSavedRecords;
    private boolean mSavedDataTab1 = true;
    private boolean mSavedDataTab2 = true;

    FragmentActivity listener;

    public static final String FRAGMENT_ARGS = "FRAGMENT_ARGS";

    private int mTabPosition;

    public static RecordsFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(FRAGMENT_ARGS, page);
        RecordsFragment fragment = new RecordsFragment();
        fragment.setArguments(args);
        return fragment;
    }


    // This event fires 1st, before creation of fragment or any views
    // The onAttach method is called when the Fragment instance is associated with an Activity.
    // This does not mean the Activity is fully initialized.
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity){
            this.listener = (FragmentActivity) context;
        }
    }

    // This event fires 2nd, before views are created for the fragment
    // The onCreate method is called when the Fragment instance is being created, or re-created.
    // Use onCreate for any standard setup that does not require the activity to be fully created
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();

        final Context context = getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                getString(R.string.user_records), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String id = "result_" + userUid; // get storage key

        // get saved games
        String result_json = sharedPreferences.getString(id,null);
        mSavedRecords = gson.fromJson(result_json, SavedRecords.class);

        // get downloaded history
        id = "downloaded_records_" + userUid;
        String history_json = sharedPreferences.getString(id,null);
        ArrayList<HistoryRecord> savedHistoryRecords = null;
        ArrayList<GameResult> savedGameResults = null;
        if (!(history_json == null)) {
            SavedRecords savedRecords = gson.fromJson(history_json, SavedRecords.class);
            savedGameResults = savedRecords.getGameRecords();
            savedHistoryRecords = savedRecords.getHistoryRecords();
        }
        id = "result_" + userUid; // get storage key


        // check if there are saved records or history
        if (mSavedRecords == null) {
            mSavedRecords = new SavedRecords();
            mSavedDataTab1 = false;
            mSavedDataTab2 = false;
        }

        mTabPosition = getArguments().getInt(FRAGMENT_ARGS);
        if(mTabPosition == 1) {
            if (!(result_json == null)){
                // if there are any history data
                if(!(mSavedRecords.getGameRecords().size() == 0)){
                    if(!(savedGameResults.size() == 0)){
                        // if history was updated
                        if (savedGameResults.size() > mSavedRecords.getGameRecords().size()){

                            mSavedRecords.setGameRecords(savedGameResults);
                            // save to shared preferences
                            result_json = gson.toJson(mSavedRecords);
                            editor.putString(id, result_json);
                            editor.commit();

                            // updated history
                            setRecordsDataToAdapter(mSavedRecords.getGameRecords());
                        } else {
                            setRecordsDataToAdapter(mSavedRecords.getGameRecords());
                        }
                    } else {
                        setRecordsDataToAdapter(mSavedRecords.getGameRecords());
                    }


                } else if (!(savedGameResults.size() == 0)){
                    mSavedRecords.setGameRecords(savedGameResults);
                    // save to shared preferences
                    result_json = gson.toJson(mSavedRecords);
                    editor.putString(id, result_json);
                    editor.commit();

                    // updated history
                    setRecordsDataToAdapter(mSavedRecords.getGameRecords());
                } else {
                    mSavedDataTab1 = false;
                }
            } else if (!(savedGameResults.size() == 0)) {
                mSavedRecords.setGameRecords(savedGameResults);
                // save to shared preferences
                result_json = gson.toJson(mSavedRecords);
                editor.putString(id, result_json);
                editor.commit();

                // updated history
                setRecordsDataToAdapter(mSavedRecords.getGameRecords());
            } else {
                mSavedDataTab1 = false;
            }

        } else {
            if (!(result_json == null)){
                // if there are any history data
                if(!(mSavedRecords.getHistoryRecords().size() == 0)){
                    if(!(savedHistoryRecords.size() == 0)){
                        // if history was updated
                        if (savedHistoryRecords.size() > mSavedRecords.getHistoryRecords().size()){

                            mSavedRecords.setHistory(savedHistoryRecords);
                            // save to shared preferences
                            result_json = gson.toJson(mSavedRecords);
                            editor.putString(id, result_json);
                            editor.commit();

                            // updated history
                            setHistoryDataToAdapter(mSavedRecords.getHistoryRecords());
                        } else {
                            setHistoryDataToAdapter(mSavedRecords.getHistoryRecords());
                        }
                    } else {
                        setHistoryDataToAdapter(mSavedRecords.getHistoryRecords());
                    }


                } else if (!(savedHistoryRecords.size() == 0)){
                    mSavedRecords.setHistory(savedHistoryRecords);
                    // save to shared preferences
                    result_json = gson.toJson(mSavedRecords);
                    editor.putString(id, result_json);
                    editor.commit();

                    // updated history
                    setHistoryDataToAdapter(mSavedRecords.getHistoryRecords());
                } else {
                    mSavedDataTab2 = false;
                }
            } else if (!(savedHistoryRecords.size() == 0)) {
                mSavedRecords.setHistory(savedHistoryRecords);
                // save to shared preferences
                result_json = gson.toJson(mSavedRecords);
                editor.putString(id, result_json);
                editor.commit();

                // updated history
                setHistoryDataToAdapter(mSavedRecords.getHistoryRecords());
            } else {
                mSavedDataTab2 = false;
            }
        }
    }

    // The onCreateView method is called when Fragment should create its View object hierarchy,
    // either dynamically or via XML layout inflation.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        if (mTabPosition == 1){
            // if there are data to show, use layout where to show them
            if (mSavedDataTab1){
                return inflater.inflate(R.layout.content_main, parent, false);
            } else {
                return inflater.inflate(R.layout.fragment_empty_screen, parent, false);
            }
        } else {
            // if there are data to show, use layout where to show them
            if (mSavedDataTab2){
                return inflater.inflate(R.layout.content_main, parent, false);
            } else {
                return inflater.inflate(R.layout.fragment_empty_screen, parent, false);
            }
        }
    }

    // This event is triggered soon after onCreateView().
    // onViewCreated() is only called if the view returned from onCreateView() is non-null.
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // if there are data to show, show them
        if (mTabPosition == 1 && mSavedDataTab1) {
            listView = view.findViewById(R.id.list);
            listView.setAdapter(mFragmentAdapterTab1);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    GameResult gameResult = mSavedRecords.getGameRecords().get(position);

                    Intent intent = new Intent(getContext(), DetailActivity.class);
                    intent.putExtra("GAME", gameResult);
                    startActivity(intent);
                }
            });
        } else if(mTabPosition != 1 && mSavedDataTab2) {
            listView = view.findViewById(R.id.list);
            listView.setAdapter(mFragmentAdapterTab2);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    HistoryRecord historyRecord = mSavedRecords.getHistoryRecords().get(position);

                    Intent intent = new Intent(getContext(), DetailActivity.class);
                    intent.putExtra("HISTORY", historyRecord);
                    startActivity(intent);
                }
            });
        }
    }

    // This method is called when the fragment is no longer connected to the Activity
    // Any references saved in onAttach should be nulled out here to prevent memory leaks.
    @Override
    public void onDetach() {
        super.onDetach();
        this.listener = null;
    }

    // This method is called after the parent Activity's onCreate() method has completed.
    // Accessing the view hierarchy of the parent activity must be done in the onActivityCreated.
    // At this point, it is safe to search for activity View objects by their ID, for example.
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void setRecordsDataToAdapter(ArrayList<GameResult> gameResults){
        for(int i = 0; i < gameResults.size(); i++) {
            fragmentModelsForRecords.add(new FragmentModel(gameResults.get(i).getDate(), gameResults.get(i).getResult() + "%"));
        }
        mFragmentAdapterTab1 = new FragmentAdapter(fragmentModelsForRecords, listener.getApplicationContext());
        mSavedDataTab1 = true;
    }

    private void setHistoryDataToAdapter(ArrayList<HistoryRecord> historyRecords){
        for(int i = 0; i < historyRecords.size(); i++) {
            fragmentModelsForHistory.add(new FragmentModel(historyRecords.get(i).getDate(), historyRecords.get(i).getResultValue() + "%"));
        }
        mFragmentAdapterTab2 = new FragmentAdapter(fragmentModelsForHistory, listener.getApplicationContext());
        mSavedDataTab2 = true;
    }
}
