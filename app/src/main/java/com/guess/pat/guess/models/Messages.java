package com.guess.pat.guess.models;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Paťo on 25.2.2018.
 */

public enum Messages {
    NOTHING("You guessed it wrong. Try again!", -1),
    ALL("Congratulation! You correctly guessed all fields.", 0),
    ONE("You correctly guessed one field.", 1),
    TWO("You correctly guessed two fields.", 2),
    THREE("You correctly guessed three fields.", 3),
    FOUR("You correctly guessed four fields.", 4),
    FIVE("You correctly guessed five fields.", 5);

    private String text;
    private int intValue;
    private static Map<Integer, Messages> map = new HashMap<Integer, Messages>();

    Messages(String message, int value){
        this.text = message;
        this.intValue = value;
    }

    static {
        for (Messages message : Messages.values()){
            map.put(message.intValue, message);
        }
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getIntValue(Messages message){
        return message.intValue;
    }

    public static Messages valueOf(int value) {
        return map.get(value);
    }

}
