package com.guess.pat.guess.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Paťo on 19.3.2018.
 */

public class DialectModel implements Serializable{

    private String name;
    private ArrayList<String> values;

    public DialectModel() {
    }

    public DialectModel(String name, ArrayList<String> values) {
        this.name = name;
        this.values = values;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getValues() {
        return values;
    }

    public void setValues(ArrayList<String> values) {
        this.values = values;
    }
}
