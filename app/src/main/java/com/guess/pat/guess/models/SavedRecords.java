package com.guess.pat.guess.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Paťo on 27.2.2018.
 */

public class SavedRecords implements Serializable {

    private ArrayList<GameResult> records = new ArrayList<>();
    private ArrayList<HistoryRecord> historyRecords = new ArrayList<>();

    public ArrayList<GameResult> getGameRecords() {
        return records;
    }

    public void setGameRecords(ArrayList<GameResult> records) {
        this.records = records;
    }

    public ArrayList<HistoryRecord> getHistoryRecords() {
        return historyRecords;
    }

    public void setHistory(ArrayList<HistoryRecord> historyRecords) {
        this.historyRecords = historyRecords;
    }
}
