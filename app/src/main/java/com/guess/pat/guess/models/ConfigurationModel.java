package com.guess.pat.guess.models;

import android.widget.ArrayAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Paťo on 13.3.2018.
 */

public class ConfigurationModel implements Serializable {

    private ArrayList<String> Moods = new ArrayList<>();
    private ArrayList<String> Ages = new ArrayList<>();
    private ArrayList<String> Education = new ArrayList<>();
    private ArrayList<String> Jobs = new ArrayList<>();
    private ArrayList<String> Countries = new ArrayList<>();
    private ArrayList<DialectModel> Dialects = new ArrayList<>();
    private ArrayList<String> Questions = new ArrayList<>();

    public ConfigurationModel() {
    }

    public ConfigurationModel(ArrayList<String> moods, ArrayList<String> ages, ArrayList<String> education,
                              ArrayList<String> jobs, ArrayList<String> countries, ArrayList<DialectModel> dialects,
                              ArrayList<String> questions) {
        Moods = moods;
        Ages = ages;
        Education = education;
        Jobs = jobs;
        Countries = countries;
        Dialects = dialects;
        Questions = questions;
    }

    public ArrayList<String> getMoods() {
        return Moods;
    }

    public void setMoods(ArrayList<String> moods) {
        Moods = moods;
    }

    public ArrayList<String> getAges() {
        return Ages;
    }

    public void setAges(ArrayList<String> ages) {
        Ages = ages;
    }

    public ArrayList<String> getEducation() {
        return Education;
    }

    public void setEducation(ArrayList<String> education) {
        Education = education;
    }

    public ArrayList<String> getCountries() {
        return Countries;
    }

    public void setCountries(ArrayList<String> countries) {
        Countries = countries;
    }

    public ArrayList<String> getJobs() {
        return Jobs;
    }

    public void setJobs(ArrayList<String> jobs) {
        Jobs = jobs;
    }

    public ArrayList<DialectModel> getDialects() {
        return Dialects;
    }

    public void setDialects(ArrayList<DialectModel> dialects) {
        Dialects = dialects;
    }

    public ArrayList<String> getQuestions() {
        return Questions;
    }

    public void setQuestions(ArrayList<String> questions) {
        Questions = questions;
    }
}
