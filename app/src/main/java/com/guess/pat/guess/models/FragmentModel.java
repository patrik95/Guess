package com.guess.pat.guess.models;

/**
 * Created by Paťo on 25.2.2018.
 */

public class FragmentModel {

    String date;
    String score;

    public FragmentModel(String date, String score) {
        this.date = date;
        this.score = score;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }
}
