package com.guess.pat.guess.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.guess.pat.guess.R;
import com.guess.pat.guess.adapters.FragmentAdapter;
import com.guess.pat.guess.adapters.FragmentTabsAdapter;
import com.guess.pat.guess.fragments.RecordsFragment;

/**
 * Created by Paťo on 25.2.2018.
 */

// Theory behind this, why and when is good to use fragments and code is from
// https://guides.codepath.com/android/creating-and-using-fragments#navigating-between-fragments

public class RecordsActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_records);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        ViewPager viewPager = findViewById(R.id.id_view_pager);
        viewPager.setAdapter(new FragmentTabsAdapter(getSupportFragmentManager(),
                RecordsActivity.this));

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = findViewById(R.id.id_sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

}
