package com.guess.pat.guess.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.guess.pat.guess.R;

public class AboutActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}
