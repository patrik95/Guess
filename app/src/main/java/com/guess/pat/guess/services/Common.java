package com.guess.pat.guess.services;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Paťo on 15.3.2018.
 */

public class Common {

    private static String baseUrl = "https://fcm.googleapis.com/";

    public static APIService getFCMClient(){
        return RetrofitClient.getClient(baseUrl).create(APIService.class);
    }

    // if device is not connected, show dialog
    public void isDeviceConnected(Context context) {
        // get connectivity info, may be null
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        // without internet connection show dialog
        if (activeNetwork == null) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Please connect to Internet to use Guess.").setTitle("No Internet")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // TODO; maybe close application, because it won't work as intended
                        }
                    });
            builder.create();
            builder.show();
        }
    }
}
