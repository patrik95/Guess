package com.guess.pat.guess.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Paťo on 28.2.2018.
 */

public class HistoryRecord implements Serializable{

    private String resultValue;
    private String date;
    ArrayList<PartResult> partResults;

    public HistoryRecord() {
    }

    public HistoryRecord(String resultValue, ArrayList<PartResult> partResults, String date) {
        this.resultValue = resultValue;
        this.partResults = partResults;
        this.date = date;
    }

    public String getResultValue() {
        return resultValue;
    }

    public void setResultValue(String resultValue) {
        this.resultValue = resultValue;
    }

    public ArrayList<PartResult> getPartResults() {
        return partResults;
    }

    public void setPartResults(ArrayList<PartResult> partResults) {
        this.partResults = partResults;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
