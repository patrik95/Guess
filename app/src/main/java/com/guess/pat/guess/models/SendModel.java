package com.guess.pat.guess.models;


/**
 * Created by Paťo on 15.3.2018.
 */

public class SendModel {
    public NotificationModel notification;
    public String to;

    public SendModel() {
    }

    public SendModel(String sendTo, NotificationModel notification) {
        this.notification = notification;
        this.to = sendTo;
    }

    public NotificationModel getNotification() {
        return notification;
    }

    public void setNotification(NotificationModel notification) {
        this.notification = notification;
    }

    public String getSendTo() {
        return to;
    }

    public void setSendTo(String sendTo) {
        this.to = sendTo;
    }
}
