package com.guess.pat.guess.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.guess.pat.guess.R;
import com.guess.pat.guess.models.GameResult;
import com.guess.pat.guess.models.HistoryRecord;

/**
 * Created by Paťo on 4.3.2018.
 */

public class DetailActivity extends AppCompatActivity {

    private TextView mMoodTextView;
    private TextView mAgeTextView;
    private TextView mEducationTextView;
    private TextView mJobTextView;
    private TextView mCountryTextView;
    private TextView mDialectTextView;
    private TextView mResult;

    private ImageView mMoodImageView;
    private ImageView mAgeImageView;
    private ImageView mEducationImageView;
    private ImageView mJobImageView;
    private ImageView mCountryImageView;
    private ImageView mDialectImageView;

    private LinearLayout mDialectLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        // getting records
        GameResult gameResult = (GameResult) getIntent().getSerializableExtra("GAME");
        HistoryRecord historyRecord = (HistoryRecord) getIntent().getSerializableExtra("HISTORY");

        mResult = findViewById(R.id.id_score);

        mDialectLayout = findViewById(R.id.id_dialect_layout);

        mMoodTextView = findViewById(R.id.id_mood_textView);
        mAgeTextView = findViewById(R.id.id_age_textView);
        mEducationTextView = findViewById(R.id.id_education_textView);
        mJobTextView = findViewById(R.id.id_job_textView);
        mCountryTextView = findViewById(R.id.id_country_textView);
        mDialectTextView = findViewById(R.id.id_dialect_textView);

        mMoodImageView = findViewById(R.id.id_mood_imageView);
        mAgeImageView = findViewById(R.id.id_age_imageView);
        mEducationImageView = findViewById(R.id.id_education_imageView);
        mJobImageView = findViewById(R.id.id_job_imageView);
        mCountryImageView = findViewById(R.id.id_country_imageView);
        mDialectImageView = findViewById(R.id.id_dialect_imageView);

        if(!(gameResult == null)) {
            for (int i = 0; i < gameResult.getPartResults().size(); i++) {
                setDetail(gameResult.getPartResults().get(i).getName(), gameResult.getPartResults().get(i).getAnswer(), gameResult.getPartResults().get(i).getPartResult());
            }
            mResult.setText(gameResult.getResult() + "%");
        } else if (!(historyRecord == null)) {
            for (int i = 0; i < historyRecord.getPartResults().size(); i++) {
                setDetail(historyRecord.getPartResults().get(i).getName(), historyRecord.getPartResults().get(i).getAnswer(), historyRecord.getPartResults().get(i).getPartResult());
            }
            mResult.setText(historyRecord.getResultValue() + "%");
        }

        Button closeButton = findViewById(R.id.id_button_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setDetail(String name, String answer, boolean result) {
        switch (name){
            case "Mood":
                mMoodTextView.setText(answer);
                mMoodImageView.setImageResource(getResultIcon(result));
            case "Age":
                mAgeTextView.setText(answer);
                mAgeImageView.setImageResource(getResultIcon(result));
                break;
            case "Education":
                mEducationTextView.setText(answer);
                mEducationImageView.setImageResource(getResultIcon(result));
                break;
            case "Job":
                mJobTextView.setText(answer);
                mJobImageView.setImageResource(getResultIcon(result));
                break;
            case "Country":
                mCountryTextView.setText(answer);
                mCountryImageView.setImageResource(getResultIcon(result));
                break;
            case "Dialect":
                mDialectTextView.setText(answer);
                mDialectImageView.setImageResource(getResultIcon(result));
                mDialectLayout.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

    private int getResultIcon (boolean result){
        if(result){
            return R.drawable.ic_checked;
        } else {
            return R.drawable.ic_cancel;
        }
    }
}
