package com.guess.pat.guess.services;

import android.app.DownloadManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.io.IOException;

/**
 * Created by Paťo on 15.3.2018.
 */

public class FirebaseInstanceIDService extends FirebaseInstanceIdService {

    private FirebaseDatabase mFirebaseDatabase;
    @Override
    public void onTokenRefresh() {
        // added for refreshing token, didn't see it work TODO: test it

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
            mFirebaseDatabase = FirebaseDatabase.getInstance();
            String token = FirebaseInstanceId.getInstance().getToken();
            mFirebaseDatabase.getReference().child("profiles").child(userUid).child("token").setValue(token);
        }
    }
}
