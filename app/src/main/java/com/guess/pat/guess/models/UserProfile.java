package com.guess.pat.guess.models;

import android.widget.ArrayAdapter;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Paťo on 2.10.2017.
 */

public class UserProfile implements Serializable {

    private String token;
    private String username;
    private String mood;
    private String age;
    private String education;
    private String job;
    private String country;
    private String dialect;
    private ArrayList<String> voiceUrls;
    private ArrayList<SpinnerItem> spinnerItems;

    public UserProfile() {
    }

    public UserProfile(String token, String username, String mood, String age, String education, String job, String country, String dialect, ArrayList<String> voiceUrls) {
        this.token = token;
        this.username = username;
        this.mood = mood;
        this.age = age;
        this.education = education;
        this.job = job;
        this.country = country;
        this.dialect = dialect;
        this.voiceUrls = voiceUrls;
        this.spinnerItems = spinnerItems;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public ArrayList<String> getVoiceUrls() {
        return voiceUrls;
    }

    public void setVoiceUrls(ArrayList<String> voiceUrls) {
        this.voiceUrls = voiceUrls;
    }

    public ArrayList<SpinnerItem> getSpinnerItems() {
        return spinnerItems;
    }

    public void setSpinnerItems(ArrayList<SpinnerItem> spinnerItems) {
        this.spinnerItems = spinnerItems;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getDialect() {
        return dialect;
    }

    public void setDialect(String dialect) {
        this.dialect = dialect;
    }
}
