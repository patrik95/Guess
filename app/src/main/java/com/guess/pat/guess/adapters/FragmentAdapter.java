package com.guess.pat.guess.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.guess.pat.guess.R;
import com.guess.pat.guess.models.FragmentModel;

import java.util.ArrayList;

/**
 * Created by Paťo on 25.2.2018.
 */

public class FragmentAdapter extends ArrayAdapter<FragmentModel> implements View.OnClickListener{

    private ArrayList<FragmentModel> dataSet;
    private int lastPosition = -1;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView date;
        TextView score;
    }

    public FragmentAdapter(ArrayList<FragmentModel> data, Context context) {
        super(context, R.layout.fragment_row_item, data);
        this.dataSet = data;
        this.mContext=context;

    }

    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        FragmentModel dataModel=(FragmentModel)object;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        FragmentModel fragmentModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tagfinal View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.fragment_row_item, parent, false);
            viewHolder.date = convertView.findViewById(R.id.date);
            viewHolder.score = convertView.findViewById(R.id.score);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        lastPosition = position;

        viewHolder.date.setText(fragmentModel.getDate());
        viewHolder.score.setText(fragmentModel.getScore());
        // Return the completed view to render on screen
        return convertView;



    }
}
