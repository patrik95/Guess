package com.guess.pat.guess.activities;


import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.guess.pat.guess.R;
import com.guess.pat.guess.models.ConfigurationModel;
import com.guess.pat.guess.models.SpinnerItem;
import com.guess.pat.guess.models.UserProfile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Paťo on 31.1.2018.
 */

public class RecordingsActivity extends AppCompatActivity {

    // firebase references for database and storage
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mProfileDatabaseReference;
    private FirebaseStorage mFirebaseStorage;
    private StorageReference mVoiceStorageReference;

    private static final String LOG_TAG = "AudioRecordTest";
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private static File mFileName = null;
    private static ArrayList<Uri> mFilePaths = new ArrayList<>();
    private static ArrayList<File> mFilePathsDownloaded = new ArrayList<>();
    private static ArrayList<StorageReference> mStorageReferences = new ArrayList<>();
    private static ArrayList<Uri> mFileDownloadUris = new ArrayList<>();
    private String mUsername;
    private boolean mUserExistsInDatabase = false;

    private ConfigurationModel mConfigurationModel;

    // record and save buttons and medias
    private RecordingsActivity.RecordButton mRecordButton = null;
    private MediaRecorder mRecorder = null;
    private RecordingsActivity.PlayButton mPlayButton = null;
    private MediaPlayer mPlayer = null;

    private Button mSaveButton = null;
    private Button mBackButton = null;
    private Button mDeleteButton = null;

    private UserProfile mSharedProfile;
    private AutoCompleteTextView mMood = null;
    private TextView mQuestion = null;

    // variables for recordings
    private int mRecordCounter = 0;
    private int mFileOrder = 0;
    private int mNumberOfRecords = 0;

    // temporary records and files ( while user deciding to save them or not )
    private File mTmpRecords = null;
    private static ArrayList<File> mTmpFiles = new ArrayList<>();
    // actual recordings
    private Spinner mRecordings;
    private ArrayList<SpinnerItem> spinnerItems = new ArrayList<>();
    private ArrayList<String> recordings_names = new ArrayList<>();
    private ArrayAdapter <ArrayList> recordings_adapter;
    // token and profile model
    private String mToken;
    private UserProfile mUserProfile = new UserProfile(null, null, null, null, null, null, null, null,new ArrayList<String>());

    // Requesting permission to RECORD_AUDIO
    private boolean permissionToRecordAccepted = false;
    private String [] permissions = {Manifest.permission.RECORD_AUDIO};

    // progress bar layouts
    private RelativeLayout mProgressBarLayout;
    private RelativeLayout mRecordingsButtonsLayout;

    // recording progress bar (play and record)
    private ProgressBar mRecordingProgress;
    private double secondPassed = 0;
    private TextView mRecordingProgressText;

    CountDownTimer mCountDownTimer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recordings);

        mUsername = getIntent().getStringExtra("USERNAME");
        mToken = FirebaseInstanceId.getInstance().getToken();

        // firebase instances and references
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mFirebaseStorage = FirebaseStorage.getInstance();

        // firebase references
        mProfileDatabaseReference = mFirebaseDatabase.getReference().child("profiles");
        mVoiceStorageReference = mFirebaseStorage.getReference().child("user_voices");

        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);

        mProgressBarLayout = findViewById(R.id.id_progress_bar_layout);
        mRecordingsButtonsLayout = findViewById(R.id.id_recordings_buttons);

        mRecordingProgress = findViewById(R.id.id_recording_progress_bar);
        mRecordingProgressText = findViewById(R.id.id_progress_bar_textView);

        // record and play buttons
        ViewGroup content = this.findViewById(R.id.id_recording_options);

        // convert dp measure to pixel for margin top in buttons
        Resources r = getBaseContext().getResources();
        int pxRight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,15, r.getDisplayMetrics());
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,4, r.getDisplayMetrics());

        // button layout parameters
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(px, px, pxRight, 0);

        mRecordButton = new RecordingsActivity.RecordButton(this);
        mRecordButton.setLayoutParams(params);
        mRecordButton.setId(R.id.button_record);
        content.addView(mRecordButton);

        params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        //params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        //params.addRule(RelativeLayout.ALIGN_BOTTOM, R.id.button_delete);
        params.setMargins(0, px, pxRight, 0);
        //params.addRule(RelativeLayout.LEFT_OF, R.id.button_delete);

        mPlayButton = new RecordingsActivity.PlayButton(this);
        mPlayButton.setLayoutParams(params);
        mPlayButton.setId(R.id.button_play);
        content.addView(mPlayButton);

        mDeleteButton = new Button(this);
        mDeleteButton.setId(R.id.button_delete);
        mDeleteButton.setText("Delete");
        params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, px, px, 0);
        mDeleteButton.setLayoutParams(params);
        content.addView(mDeleteButton);



        // setting spinner items
        setSpinerItems();

        // get configuration
        getConfiguration();

        // save profile button
        mSaveButton = findViewById(R.id.button_save );
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // show progress bar and gray down background
                mProgressBarLayout.setVisibility(View.VISIBLE);
                mRecordingsButtonsLayout.setAlpha((float) 0.5);
                mRecordingsButtonsLayout.setClickable(false);

                // delete storage with voices and save new voices + update profile
                mProfileDatabaseReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (snapshot.exists()) {
                            // delete
                            mUserExistsInDatabase = true;
                            UserProfile profile = snapshot.getValue(UserProfile.class);
                            if(!(profile.getVoiceUrls() == null)){
                                deleteStorage();
                            } else {
                                saveProfile();
                            }

                        } else {
                            // create new profile
                            mUserExistsInDatabase = false;
                            saveProfile();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
        });

        mDeleteButton = findViewById(R.id.button_delete);
        mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!mRecordings.getSelectedItem().toString().equals("<Empty>")){
                    recordings_names.set(mRecordings.getSelectedItemPosition(),"<Empty>");
                    spinnerItems.get(mRecordings.getSelectedItemPosition()).setStorageReference(null);
                    spinnerItems.get(mRecordings.getSelectedItemPosition()).setFilePath(null);
                    spinnerItems.get(mRecordings.getSelectedItemPosition()).setName("<Empty>");
                    recordings_adapter.notifyDataSetChanged();
                    stopRecordingOrPlaying();
                }
                stopRecordingOrPlaying();
            }
        });

        mBackButton = this.findViewById(R.id.button_back );
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                stopRecordingOrPlaying();
                finish();
            }
        });

        mQuestion = findViewById(R.id.id_questions_textView);
        Random randomGenerator = new Random();
        int questionIndex =  randomGenerator.nextInt(mConfigurationModel.getQuestions().size());
        mQuestion.setText(mConfigurationModel.getQuestions().get(questionIndex));

        ImageView changeQuestion = findViewById(R.id.id_change_question);
        changeQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Random randomGenerator = new Random();
                int questionIndex =  randomGenerator.nextInt(mConfigurationModel.getQuestions().size());
                mQuestion.setText(mConfigurationModel.getQuestions().get(questionIndex));
            }
        });

        loadStoredData();
    }

    private void stopRecordingOrPlaying (){
        if (!(mCountDownTimer == null)) {
            mCountDownTimer.cancel();
        }
        if (!(mPlayer == null)){
            if (mPlayer.isPlaying()){
                mPlayer.stop();
                mPlayButton.mStartPlaying = !mPlayButton.mStartPlaying;
                mPlayButton.setText("Play");
            }
        }
        if (!(mRecorder == null) && !mRecordButton.mStartRecording) {
            mRecorder.release();
            mRecordButton.mStartRecording = !mRecordButton.mStartRecording;
            mRecordButton.setText("Record");
        }
        mRecordingProgress.setProgress(0);
        mRecordingProgressText.setText("0s");
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopRecordingOrPlaying();
    }

    // Setting spinner items
    private void setSpinerItems() {
        int numberOfSpinnerItems = 3; // TODO: get from config
        mRecordings = findViewById(R.id.recordings_menu);

        for (int i = 0; i < numberOfSpinnerItems; i++){
            recordings_names.add("<Empty>");
            SpinnerItem item = new SpinnerItem(i, "<Empty>", null);
            spinnerItems.add(item);
        }

        recordings_adapter = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, recordings_names);
        mRecordings.setAdapter(recordings_adapter);
        // set progress bar and text to default values when recording change
        mRecordings.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // reset
                mRecordingProgress.setProgress(0);
                mRecordingProgressText.setText("0s");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });
    }

    private void getConfiguration () {
        Gson gson = new Gson();
        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.configuration), Context.MODE_PRIVATE);

        String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String id = "configuration_" + userUid; // get storage key

        String user_json = sharedPref.getString(id,null);
        mConfigurationModel  = gson.fromJson(user_json, ConfigurationModel.class);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_RECORD_AUDIO_PERMISSION:
                permissionToRecordAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!permissionToRecordAccepted ) finish();

    }

    // checking if user has files in storage
    private void deleteStorage(){
        String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();

        mProfileDatabaseReference.child(userUid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mUserProfile = dataSnapshot.getValue(UserProfile.class);

                if(!(mUserProfile.getVoiceUrls() == null)) {
                    deleteStoredFiles(0, mUserProfile.getVoiceUrls());
                } else {
                    saveProfile();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    // deleting files in remote storage
    private void deleteStoredFiles(int i, final ArrayList<String> filesToDelete){
        final int index = i;
        String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();

        // get filename from string path
        int lastIndex = filesToDelete.get(index).indexOf("alt=") - 1;
        int startIndex = filesToDelete.get(index).indexOf("Voice_");
        String fileName  = filesToDelete.get(index).substring(startIndex , lastIndex);

        StorageReference filepath = mVoiceStorageReference.child(userUid).child(fileName);
        filepath.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                int nextIndex = index + 1;
                if(nextIndex >= filesToDelete.size()) {
                    saveProfile();
                } else {
                    deleteStoredFiles(nextIndex, filesToDelete);
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });

    }

    private void saveProfile() {
        // upload files and update profile
        uploadFiles(0, spinnerItems.size());
    }

    private void uploadFiles(int i, int count){
        final int index = i;
        final int maxCount = count;
        if (!(spinnerItems.get(index).getName().equals("<Empty>"))) {
            spinnerItems.get(index).getStorageReference()
                    .putFile(Uri.parse(spinnerItems.get(index).getFilePath())).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    mFileDownloadUris.add(downloadUrl);
                    int nextIndex = index + 1;
                    if (!(nextIndex >= maxCount)) {
                        uploadFiles(nextIndex, maxCount);
                    } else {
                        updateProfile();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(RecordingsActivity.this, "Failure durring upload occured. Try again.", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            int nextIndex = index + 1;
            if (!(nextIndex >= maxCount)) {
                uploadFiles(nextIndex, maxCount);
            } else {
                updateProfile();
            }
        }
    }

    // update profile in database
    private void updateProfile() {
        String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        ArrayList<String> filePaths = uriArrayToStringArray(mFileDownloadUris);
        mNumberOfRecords = filePaths.size();
        UserProfile newUserProfile = null;

        if (!(mNumberOfRecords == 0)) {
            // tmp files deletion
            if(!(mTmpRecords == null)){
                deleteRecursive(mTmpRecords.getParentFile());
            }

            if (mUserExistsInDatabase) {
                // edit existing profile
                mProfileDatabaseReference.child(userUid).child("mood").setValue(mMood.getText().toString());
                mProfileDatabaseReference.child(userUid).child("voiceUrls").removeValue();
                mProfileDatabaseReference.child(userUid).child("voiceUrls").setValue(filePaths);
            } else {
                // create new profile
                newUserProfile = new UserProfile(mToken, mUsername, mMood.getText().toString(),
                        null, null, null, null, null, filePaths);
                mProfileDatabaseReference.child(userUid).setValue(newUserProfile);
            }
            mRecordCounter = 0;
            mFileOrder = 0;
            mFileDownloadUris.clear();
            mStorageReferences.clear();
            mFilePaths.clear();
            if (!mUserProfile.getVoiceUrls().isEmpty()) {
                mUserProfile.getVoiceUrls().clear();
            }

            File rootPath = new File(getExternalFilesDir("user_records"), mUsername);
            /*if(!rootPath.exists()) {
                rootPath.mkdirs();
            }*/
            deleteRecursive(rootPath);
            rootPath.delete();
            rootPath.mkdirs();
            downloadFiles(0);
        } else {
            if (mUserExistsInDatabase) {
                // edit existing profile
                mProfileDatabaseReference.child(userUid).child("mood").setValue(mMood.getText().toString());
                mProfileDatabaseReference.child(userUid).child("voiceUrls").removeValue();
            } else {
                // create new profile
                newUserProfile = new UserProfile(mToken, mUsername, mMood.getText().toString(),
                        null, null, null, null, null, null);
                mProfileDatabaseReference.child(userUid).setValue(newUserProfile);
            }
            mRecordCounter = 0;
            mFileOrder = 0;
            mFileDownloadUris.clear();
            mStorageReferences.clear();
            mFilePaths.clear();

            // in case of user deleted all recordings, delete his file from device
            File rootPath = new File(getExternalFilesDir("user_records"), mUsername);
            deleteRecursive(rootPath);
            rootPath.delete();
            if (!mUserProfile.getVoiceUrls().isEmpty()) {
                mUserProfile.getVoiceUrls().clear();
            }
        }

        // convert User object user to JSON format
        Gson gson = new Gson();
        if (!(mMood == null)) {
            if (!(mSharedProfile == null) && newUserProfile == null){
                mSharedProfile.setMood(mMood.getText().toString());
            } else {
                mSharedProfile = newUserProfile;
            }
        }

        String user_json = gson.toJson(mSharedProfile);

        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.user_profile), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        // store in SharedPreferences
        String id = "profile_" + userUid; // get storage key

        editor.putString(id, user_json);
        editor.commit();

        mProgressBarLayout.setVisibility(View.INVISIBLE);
        mRecordingsButtonsLayout.setClickable(true);
        mRecordingsButtonsLayout.setAlpha(1);
        finish();
    }

    private ArrayList<String> uriArrayToStringArray(ArrayList<Uri> uriArray) {
        ArrayList<String> stringArray = new ArrayList<>();
        for (int i = 0; i < uriArray.size(); i++) {
            stringArray.add(uriArray.get(i).toString());
        }
        return stringArray;
    }

    // recursivly deleting file/directory
    public void deleteRecursive(File fileOrDirectory) {

        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                deleteRecursive(child);
            }
        }
        fileOrDirectory.delete();
    }

    private void downloadFiles(int i) {
        final int index = i;
        if(!(spinnerItems.get(index).getName().equals("<Empty>"))){
            StorageReference filepath = mVoiceStorageReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(spinnerItems.get(index).getName());

            File rootPath = new File(getExternalFilesDir("user_records"), mUsername);
            File localFile = new File(rootPath,spinnerItems.get(index).getName()+".3gp");
            mUserProfile.getVoiceUrls().add(localFile.getAbsolutePath());
            // mFilePathsDownloaded.add(localFile);

            spinnerItems.get(index).setFilePath(Uri.fromFile(localFile).toString());
            spinnerItems.get(index).setStorageReference(filepath);

            filepath.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    mFileOrder++;
                    if (mFileOrder >= mNumberOfRecords){
                        mFileOrder = 0;
                        mRecordingProgress.setProgress(0);
                        mRecordingProgressText.setText("0s");
                    } else {
                        int nextIndex = index + 1;
                        downloadFiles(nextIndex);
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                }
            });
        } else {
            int nextIndex = index + 1;
            downloadFiles(nextIndex);
        }
    }

    private void loadStoredData() {
        final File file = new File(getExternalFilesDir("user_records"), mUsername);
        if(file.exists()){
            getStoredFiles(file);
        }
        setMood();
    }

    // get profile and set autoCompleteView
    private void setMood(){
        Gson gson = new Gson();
        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.user_profile), Context.MODE_PRIVATE);

        String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String id = "profile_" + userUid; // get storage key

        String user_json = sharedPref.getString(id,"");
        mSharedProfile  = gson.fromJson(user_json, UserProfile.class);

        // MOOD
        mMood = findViewById(R.id.autoCompleteMood);
        // array adapter for autocomplete view
        ArrayAdapter<String> mood_adapter = new ArrayAdapter<String>(this,
                R.layout.custom_dropdown, mConfigurationModel.getMoods());

        mMood.setAdapter(mood_adapter);
        mMood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                mMood.showDropDown();
            }
        });

        if (!(mSharedProfile == null)){
            mMood.setListSelection(mood_adapter.getPosition(mSharedProfile.getMood()));
            mMood.setText(mSharedProfile.getMood());
        }
    }

    //TODO: save whole profile in device, store and restore it in same order (recordings)
    // get stored files
    public void getStoredFiles(File fileOrDirectory) {
        int i = 0;
        if (fileOrDirectory.isDirectory()) {
            mUserProfile.getVoiceUrls().clear();
            for (File child : fileOrDirectory.listFiles()) {
                // get reference of file in firebse
                StorageReference filepath = mVoiceStorageReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid().toString()).child(child.getName().substring(0,7));
                mUserProfile.getVoiceUrls().add(child.getAbsolutePath());

                spinnerItems.get(i).setOrderNumber(i);
                spinnerItems.get(i).setName(child.getName().substring(0,7));
                spinnerItems.get(i).setFilePath(Uri.fromFile(child).toString());
                spinnerItems.get(i).setStorageReference(filepath);

                recordings_names.set(i,child.getName().substring(0,7));
                recordings_adapter.notifyDataSetChanged();
                i++;
            }
        }
    }

    /*********** Recording and playing audio **************/
    class RecordButton extends AppCompatButton{
        boolean mStartRecording = true;

        OnClickListener clicker = new OnClickListener() {
            public void onClick(View v) {
                // Record to the external cache directory for visibility
                mTmpRecords = new File(new File(getExternalFilesDir("user_records"),"tmp_user_records" ), mUsername);
                mTmpRecords.mkdirs();
                int fileNumber = mRecordings.getSelectedItemPosition() +1;
                mFileName = new File(mTmpRecords,"Voice_"+ fileNumber +".3gp");
                onRecord(mStartRecording);
                if (mStartRecording) {
                    setText("Stop");
                } else {
                    setText("Record");
                    // adding user's records to array
                    StorageReference filepath = mVoiceStorageReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid().toString()).child("Voice_"+fileNumber);
                    mStorageReferences.add(filepath);
                    mTmpFiles.add(mFileName);
                    mFilePaths.add(Uri.fromFile(mFileName));

                    // change in recording's list
                    recordings_names.set(mRecordings.getSelectedItemPosition(),"Voice_"+fileNumber);
                    recordings_adapter.notifyDataSetChanged();
                    spinnerItems.get(mRecordings.getSelectedItemPosition()).setName("Voice_"+fileNumber);
                    spinnerItems.get(mRecordings.getSelectedItemPosition()).setFilePath(Uri.fromFile(mFileName).toString());
                    spinnerItems.get(mRecordings.getSelectedItemPosition()).setStorageReference(filepath);
//                    mRecordings.setSelection(mRecordings.getSelectedItemPosition());
//                    mRecordings.setAdapter(recordings_adapter);

                    mRecordCounter++;
                }
                mStartRecording = !mStartRecording;
            }
        };

        public RecordButton(Context ctx) {
            super(ctx);
            setText("Record");
            setOnClickListener(clicker);
        }
    }

    private void onRecord(boolean start) {
        if (start) {
            startRecording();
        } else {
            mCountDownTimer.cancel();
            stopRecording();
        }
    }

    private void startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(mFileName.getPath());
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        mRecorder.setMaxDuration(30000); // (30 seconds) in ms

        try {
            mRecorder.prepare();
        } catch (IOException e) {
        }

        mRecorder.start();

        mRecordingProgress = findViewById(R.id.id_recording_progress_bar);
        mRecordingProgress.setProgress(0);
        mRecordingProgress.setMax(30); // set to 30 seconds

        mRecordingProgressText = findViewById(R.id.id_progress_bar_textView);
        mRecordingProgressText.setText("0s");

        // count down timer in seconds, it never should go to end, but each tick will update text and progress
        mCountDownTimer = new CountDownTimer(31000, 500) {

            @Override
            public void onTick(long millisUntilFinished) {
                secondPassed += 0.5;
                if(secondPassed == 1){
                    secondPassed = 0;
                    mRecordingProgressText.setText(mRecordingProgress.getProgress() + 1 + "s");
                    mRecordingProgress.setProgress(mRecordingProgress.getProgress() + 1);
                }
            }

            @Override
            public void onFinish() {
            }
        };
        mCountDownTimer.start();

        mRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
            @Override
            public void onInfo(MediaRecorder mediaRecorder, int i, int i1) {

                if (i == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
                    mediaRecorder.stop();
                    mediaRecorder.release();
                    mRecorder = null;

                    mRecordButton.setText("Record");
                    // adding user's records to array
                    int fileNumber = mRecordings.getSelectedItemPosition() +1;
                    StorageReference filepath = mVoiceStorageReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid().toString()).child("Voice_"+fileNumber);
                    mStorageReferences.add(filepath);
                    mTmpFiles.add(mFileName);
                    mFilePaths.add(Uri.fromFile(mFileName));

                    // change in recording's list
                    recordings_names.set(mRecordings.getSelectedItemPosition(),"Voice_"+fileNumber);
                    recordings_adapter.notifyDataSetChanged();
                    spinnerItems.get(mRecordings.getSelectedItemPosition()).setName("Voice_"+fileNumber);
                    spinnerItems.get(mRecordings.getSelectedItemPosition()).setFilePath(Uri.fromFile(mFileName).toString());
                    spinnerItems.get(mRecordings.getSelectedItemPosition()).setStorageReference(filepath);
                    mRecordButton.mStartRecording = !mRecordButton.mStartRecording;
                    mCountDownTimer.cancel();
                }
            }
        });

    }

    private void stopRecording() {
        mRecorder.release();
        mRecorder = null;
    }

    class PlayButton extends AppCompatButton {
        boolean mStartPlaying = true;

        View.OnClickListener clicker = new View.OnClickListener() {
            public void onClick(View v) {
                if(!(mRecordings.getSelectedItem().toString().equals("<Empty>"))){
                    onPlay(mStartPlaying);
                    if (mStartPlaying) {
                        setText("Stop");
                    } else {
                        setText("Play");
                    }
                    mStartPlaying = !mStartPlaying;
                }
            }
        };

        public PlayButton(Context ctx) {
            super(ctx);
            setText("Play");
            setOnClickListener(clicker);
        }
    }

    private void onPlay(boolean start) {
        if (start) {
            startPlaying();
        } else {
            mCountDownTimer.cancel();
            stopPlaying();
        }
    }

    private void startPlaying() {
        mPlayer = new MediaPlayer();

        try {
            if(!(spinnerItems.size() == 0)){
                mPlayer.setDataSource(spinnerItems.get(mRecordings.getSelectedItemPosition()).getFilePath().toString());
                mPlayer.prepare();
                mPlayer.start();

                // getting duration of recording
                Uri uri = Uri.parse(spinnerItems.get(mRecordings.getSelectedItemPosition()).getFilePath());
                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                mmr.setDataSource(this,uri);
                String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                int millSecond = Integer.parseInt(durationStr);
                final int seconds = Math.round(millSecond/1000);

                // set progressbar
                secondPassed  = 0;
                mRecordingProgress = findViewById(R.id.id_recording_progress_bar);
                mRecordingProgress.setProgress(0);
                mRecordingProgress.setMax(seconds);

                mRecordingProgressText = findViewById(R.id.id_progress_bar_textView);
                mRecordingProgressText.setText("0s");

                // count down timer in seconds
                mCountDownTimer = new CountDownTimer(30000, 500) {

                    @Override
                    public void onTick(long millisUntilFinished) {
                        // Nothing to do
                        if(!(mPlayer == null)){
                            if (!mPlayer.isPlaying()) {
                                mPlayButton.setText("Play");
                                mPlayer.stop();
                                stopPlaying();
                                mPlayButton.mStartPlaying = !mPlayButton.mStartPlaying;
                                mCountDownTimer.cancel();

                            }
                            secondPassed += 0.5;
                            if(secondPassed == 1){
                                secondPassed = 0;
                                mRecordingProgressText.setText(mRecordingProgress.getProgress() + 1 + "s");
                                mRecordingProgress.setProgress(mRecordingProgress.getProgress() + 1);
                            }
                        }
                    }

                    @Override
                    public void onFinish() {
                        if(!(mPlayer == null)) {
                            if (mPlayer.isPlaying()) {
                                mPlayButton.setText("Play");
                                mPlayer.stop();
                                stopPlaying();
                                mPlayButton.mStartPlaying = !mPlayButton.mStartPlaying;
                                mRecordingProgress.setProgress(mRecordingProgress.getMax());
                                mRecordingProgressText.setText(mRecordingProgress.getMax() + "s");

                            }
                        }
                    }
                };
                mCountDownTimer.start();
            } else {
                mPlayer.setDataSource(mUserProfile.getVoiceUrls().get(0));
                mPlayer.prepare();
                mPlayer.start();
                // count down timer in seconds
                mCountDownTimer = new CountDownTimer(30000, 5000) {

                    @Override
                    public void onTick(long millisUntilFinished) {
                        // Nothing to do
                    }

                    @Override
                    public void onFinish() {
                        if (mPlayer.isPlaying()) {
                            mPlayButton.setText("Play");
                            mPlayer.stop();
                            stopPlaying();
                            mPlayButton.mStartPlaying = !mPlayButton.mStartPlaying;
                        }
                    }
                };
                mCountDownTimer.start();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void stopPlaying() {
        mPlayer.release();
        mPlayer = null;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
        }

        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
    }

}
