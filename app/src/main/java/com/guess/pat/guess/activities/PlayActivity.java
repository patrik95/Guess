package com.guess.pat.guess.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.guess.pat.guess.R;
import com.guess.pat.guess.models.ConfigurationModel;
import com.guess.pat.guess.models.DialectModel;
import com.guess.pat.guess.models.GameResult;
import com.guess.pat.guess.models.PartResult;
import com.guess.pat.guess.models.SpinnerItem;
import com.guess.pat.guess.models.UserProfile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Paťo on 21.2.2018.
 */

public class PlayActivity extends AppCompatActivity {

    private String mGuessed_user_uid;
    private UserProfile mGuessProfile = new UserProfile();
    private ArrayList<SpinnerItem> spinnerItems;
    private ConfigurationModel mConfigurationModel;

    private PlayActivity.PlayButton mPlayButton = null;
    private Button mSubmit = null;

    // media player
    private MediaPlayer mPlayer;
    // timer
    private CountDownTimer mCountDownTimer;

    // spinners
    private Spinner mRecordings;
    private AutoCompleteTextView mAge;
    private AutoCompleteTextView mEducation;
    private AutoCompleteTextView mJob;
    private AutoCompleteTextView mCountry;
    private AutoCompleteTextView mDialect;
    private AutoCompleteTextView mMood;

    private LinearLayout mDialectLayout;

    // recording progress bar
    private ProgressBar mRecordingProgress;
    private double secondPassed = 0;
    private TextView mRecordingProgressText;

    private Context mContext;
    private DatabaseReference mProfileDatabaseReference;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        mContext = this;

        // firebase database
        mProfileDatabaseReference = FirebaseDatabase.getInstance().getReference().child("reports");

        // getting profile to guess
        mGuessProfile = (UserProfile) getIntent().getSerializableExtra("PROFILE");
        mGuessed_user_uid = getIntent().getStringExtra("GUESSEDUID");
        spinnerItems = mGuessProfile.getSpinnerItems();

        // set recordings to spinner
        setSpinerItems();

        // get configuration and set autocomplete views
        getConfiguration();
        setAutoCompleteViews();

        mSubmit = findViewById(R.id.button_submit);
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calculateResult();
            }
        });

        // convert dp measure to pixel for margin top in buttons
        Resources r = getBaseContext().getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,4, r.getDisplayMetrics());
        int pxRight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,30, r.getDisplayMetrics());

        // play button
        RelativeLayout content = this.findViewById(R.id.id_navigation_layout);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(px,px,pxRight,0);
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

        mPlayButton = new PlayActivity.PlayButton(this);
        mPlayButton.setLayoutParams(params);
        content.addView(mPlayButton);

        ImageView imageView = findViewById(R.id.exclamation_mark);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage("Are you sure, that user abuses this service with his recordings?").setTitle("Report user")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // calling cloud function sendMail
                                mProfileDatabaseReference.child(mGuessed_user_uid).child("reported_by")
                                        .setValue(FirebaseAuth.getInstance().getCurrentUser().getUid().toString());
                                mProfileDatabaseReference.child(mGuessed_user_uid).child("checked")
                                        .setValue("false");
                                //
                                Intent i = new Intent(Intent.ACTION_SEND);
                                i.setType("message/rfc822");
                                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"guessreports@gmail.com"});
                                i.putExtra(Intent.EXTRA_SUBJECT, "User report");
                                i.putExtra(Intent.EXTRA_TEXT   , "User with " + mGuessed_user_uid + " uid is abusing application's recordings service.");
                                try {
                                    startActivity(Intent.createChooser(i, "Send report..."));
                                } catch (android.content.ActivityNotFoundException ex) {
                                    Toast.makeText(mContext, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                                }

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                builder.create();
                builder.show();
            }
        });

    }
    @Override
    protected void onPause() {
        super.onPause();
        if (!(mCountDownTimer == null)) {
            mCountDownTimer.cancel();
        }
        if (!(mPlayer == null)){
            if (mPlayer.isPlaying()){
                mPlayer.stop();
                mPlayButton.mStartPlaying = !mPlayButton.mStartPlaying;
                mPlayButton.setText("Play");
            }
        }
        if (!(mRecordingProgress == null)){
            mRecordingProgress.setProgress(0);
            mRecordingProgressText.setText("0s");
        }
    }

    // calculate game result and go to next screen (Result)
    private void calculateResult() {

        mPlayButton.setText("Play");
        if (!(mPlayer == null)){
            mPlayer.stop();
        }

        GameResult gameResult = new GameResult();
        PartResult partResult;

        Boolean result = mMood.getText().toString().equals(mGuessProfile.getMood());
        partResult = new PartResult("Mood", mMood.getText().toString(), result);
        gameResult.getPartResults().add(partResult);

        result = mAge.getText().toString().equals(mGuessProfile.getAge());
        partResult = new PartResult("Age", mAge.getText().toString(), result);
        gameResult.getPartResults().add(partResult);

        result = mEducation.getText().toString().equals(mGuessProfile.getEducation());
        partResult = new PartResult("Education", mEducation.getText().toString(), result);
        gameResult.getPartResults().add(partResult);

        result = mJob.getText().toString().equals(mGuessProfile.getJob());
        partResult = new PartResult("Job", mJob.getText().toString(), result);
        gameResult.getPartResults().add(partResult);

        result = mCountry.getText().toString().equals(mGuessProfile.getCountry());
        partResult = new PartResult("Country", mCountry.getText().toString(), result);
        gameResult.getPartResults().add(partResult);

        if (!mDialect.getText().toString().equals("None") && !mDialect.getText().toString().equals("")){
            result = mDialect.getText().toString().equals(mGuessProfile.getDialect());
            partResult = new PartResult("Dialect", mDialect.getText().toString(), result);
            gameResult.getPartResults().add(partResult);
        }

        gameResult.setResultNumber(-1); // set it to all wrong answers

        // get the result or keep it to all wrong
        int count = 0;
        for (int i = 0; i < gameResult.getPartResults().size(); i++) {
            if (gameResult.getPartResults().get(i).getPartResult()){
                if (!gameResult.getPartResults().get(i).getName().equals("Dialect")){
                    count++;
                    gameResult.setResultNumber(count); // message is set automatically
                } else {
                    if (gameResult.getPartResults().get(i).getPartResult() && !gameResult.getPartResults().get(i).getAnswer().equals("None")){
                        gameResult.getMessage().setText(gameResult.getMessage().getText() + " You got dialect right!");
                    }
                }
//                gameResult.setMessage(Messages.valueOf(gameResult.getResult()));
            }
        }
        // delete temporary recordings
        deleteRecursive(new File(getExternalFilesDir("user_records"), "tmp_downloaded_records"));
        // open new activity (ResultActivity)
        Intent intent = new Intent(getBaseContext(), ResultActivity.class);
        intent.putExtra("RESULT", gameResult);
            intent.putExtra("GUESSEDUID", mGuessed_user_uid);
            intent.putExtra("DEVICETOKEN", mGuessProfile.getToken());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();

    }

    // set recordings to spinner
    private void setSpinerItems() {
        int numberOfSpinnerItems = spinnerItems.size();
        mRecordings = findViewById(R.id.spinner_recordings);
        ArrayList<String> recordings_names = new ArrayList<>();
        ArrayAdapter<ArrayList> recordings_adapter;

        for (int i = 0; i < numberOfSpinnerItems; i++){
            recordings_names.add(spinnerItems.get(i).getName());
        }

        recordings_adapter = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, recordings_names);
        mRecordings.setAdapter(recordings_adapter);
    }

    // recursivly deleting file/directory
    public void deleteRecursive(File fileOrDirectory) {

        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                deleteRecursive(child);
            }
        }
        fileOrDirectory.delete();
    }

    private void getConfiguration () {
        Gson gson = new Gson();
        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.configuration), Context.MODE_PRIVATE);

        String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String id = "configuration_" + userUid; // get storage key

        String user_json = sharedPref.getString(id,null);
        mConfigurationModel  = gson.fromJson(user_json, ConfigurationModel.class);
    }

    // get data from default values or from database
    private void setAutoCompleteViews() {
        if (mConfigurationModel == null){
            // AGE
            mAge = findViewById(R.id.autoTextAge);
            // array adapter for autocomplete view
            ArrayAdapter<CharSequence> age_adapter = ArrayAdapter.createFromResource(this,
                    R.array.age, android.R.layout.simple_spinner_item);
            age_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            mAge.setAdapter(age_adapter);
            mAge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mAge.showDropDown();
                }
            });

            // EDUCATION
            mEducation = findViewById(R.id.spinnerEducation);
            // array adapter for autocomplete view
            ArrayAdapter<CharSequence> education_adapter = ArrayAdapter.createFromResource(this,
                    R.array.education, android.R.layout.simple_spinner_item);
            education_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mEducation.setAdapter(education_adapter);
            mEducation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mEducation.showDropDown();
                }
            });

            // JOBS
            mJob = findViewById(R.id.autoComplete_jobs);
            // array adapter for autocomplete view
            ArrayAdapter<CharSequence> jobs_adapter = ArrayAdapter.createFromResource(this,
                    R.array.countries, android.R.layout.simple_spinner_item);
            jobs_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mJob.setAdapter(jobs_adapter);

            mJob.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mJob.showDropDown();
                }
            });

            // COUNTRY NAMES
            mCountry = findViewById(R.id.spinnerCountry);
            // array adapter for autocomplete view
            ArrayAdapter<CharSequence> country_adapter = ArrayAdapter.createFromResource(this,
                    R.array.countries, android.R.layout.simple_spinner_item);
            country_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mCountry.setAdapter(country_adapter);

            mCountry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mCountry.showDropDown();
                }
            });

            // DIALECT
            mDialect = findViewById(R.id.autoComplete_dialect);
            // array adapter for autocomplete view
            ArrayAdapter<CharSequence> dialect_adapter = ArrayAdapter.createFromResource(this,
                    R.array.dialect, android.R.layout.simple_spinner_item);
            dialect_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mDialect.setAdapter(dialect_adapter);

            mDialect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mDialect.showDropDown();
                }
            });
        } else {

            // MOOD
            mMood = findViewById(R.id.autoTextMood);
            // array adapter for autocomplete view
            ArrayAdapter<String> mood_adapter = new ArrayAdapter<String>(this,
                    R.layout.custom_dropdown, mConfigurationModel.getMoods());

            mMood.setAdapter(mood_adapter);
            mMood.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mMood.showDropDown();
                }
            });

            // AGE
            mAge = findViewById(R.id.autoTextAge);
            // array adapter for autocomplete view
            ArrayAdapter<String> age_adapter = new ArrayAdapter<String>(this,
                    R.layout.custom_dropdown, mConfigurationModel.getAges());

            mAge.setAdapter(age_adapter);
            mAge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mAge.showDropDown();
                }
            });

            // EDUCATION
            mEducation = findViewById(R.id.spinnerEducation);
            // array adapter for autocomplete view
            ArrayAdapter<String> education_adapter = new ArrayAdapter<String>(this,
                    R.layout.custom_dropdown, mConfigurationModel.getEducation());
            mEducation.setAdapter(education_adapter);
            mEducation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mEducation.showDropDown();
                }
            });

            // JOBS
            mJob = findViewById(R.id.autoComplete_jobs);
            // array adapter for autocomplete view
            ArrayAdapter<String> jobs_adapter = new ArrayAdapter<String>(this,
                    R.layout.item_dropdown, R.id.item, mConfigurationModel.getJobs());
            mJob.setAdapter(jobs_adapter);

            mJob.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mJob.showDropDown();
                }
            });

            //COUNTRY NAMES
            mCountry = findViewById(R.id.spinnerCountry);
            // array adapter for autocomplete view
            ArrayAdapter<String> country_adapter = new ArrayAdapter<String>(this,
                    R.layout.item_dropdown, R.id.item, mConfigurationModel.getCountries());
            mCountry.setAdapter(country_adapter);

            mCountry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mCountry.showDropDown();
                }
            });
            mCountry.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String country = mCountry.getText().toString();
                    ArrayList<DialectModel> dialects = mConfigurationModel.getDialects();
                    ArrayAdapter<String> dialect_adapter;
                    if (country.equals("Slovakia")){
                        dialect_adapter = new ArrayAdapter<String>(PlayActivity.this,
                                R.layout.custom_dropdown, dialects.get(2).getValues());
                        mDialectLayout.setVisibility(View.VISIBLE);
                    } else if (country.equals("Czech Republic")){
                        dialect_adapter = new ArrayAdapter<String>(PlayActivity.this,
                                R.layout.custom_dropdown, dialects.get(1).getValues());
                        mDialectLayout.setVisibility(View.VISIBLE);
                    } else {
                        dialect_adapter = new ArrayAdapter<String>(PlayActivity.this,
                                R.layout.custom_dropdown, dialects.get(0).getValues());
                        mDialectLayout.setVisibility(View.GONE);
                    }

                    mDialect.setAdapter(dialect_adapter);
                }
            });

            // DIALECT
            mDialectLayout = findViewById(R.id.id_dialect_layout);
            mDialect = findViewById(R.id.autoComplete_dialect);
            // array adapter for autocomplete view
            ArrayAdapter<String> dialect_adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, mConfigurationModel.getDialects().get(0).getValues());
            dialect_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mDialect.setAdapter(dialect_adapter);

            mDialect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mDialect.showDropDown();
                }
            });
        }
    }

    /*************  Play button **************/
    class PlayButton extends AppCompatButton {
        boolean mStartPlaying = true;

        View.OnClickListener clicker = new View.OnClickListener() {
            public void onClick(View v) {
                onPlay(mStartPlaying);
                if (mStartPlaying) {
                    setText("Stop");
                } else {
                    setText("Play");
                }
                mStartPlaying = !mStartPlaying;
            }
        };

        public PlayButton(Context ctx) {
            super(ctx);
            setText("Play");
            setOnClickListener(clicker);
        }
    }

    private void onPlay(boolean start) {
        if (start) {
            startPlaying();
        } else {
            mCountDownTimer.cancel();
            stopPlaying();
        }
    }

    // play functionality
    private void startPlaying() {
        mPlayer = new MediaPlayer();

        try {
            if(!(spinnerItems.size() == 0)){
                mPlayer.setDataSource(spinnerItems.get(mRecordings.getSelectedItemPosition()).getFilePath().toString());
                mPlayer.prepare();
                mPlayer.start();

                // getting duration of recording
                Uri uri = Uri.parse(spinnerItems.get(mRecordings.getSelectedItemPosition()).getFilePath());
                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                mmr.setDataSource(this,uri);
                String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                int millSecond = Integer.parseInt(durationStr);
                final int seconds = Math.round(millSecond/1000);

                // set progressbar
                secondPassed  = 0;
                mRecordingProgress = findViewById(R.id.id_recording_progress_bar);
                mRecordingProgress.setProgress(0);
                mRecordingProgress.setMax(seconds);

                mRecordingProgressText = findViewById(R.id.id_progress_bar_textView);
                mRecordingProgressText.setText("0s");


                // count down timer in seconds
                mCountDownTimer = new CountDownTimer(30000, 500) {

                    @Override
                    public void onTick(long millisUntilFinished) {
                        // Nothing to do
                        if (!mPlayer.isPlaying()) {
                            mPlayButton.setText("Play");
                            mPlayer.stop();
                            stopPlaying();
                            mPlayButton.mStartPlaying = !mPlayButton.mStartPlaying;
                            mCountDownTimer.cancel();
                        }

                        secondPassed += 0.5;
                        if(secondPassed == 1){
                            secondPassed = 0;
                            mRecordingProgressText.setText(mRecordingProgress.getProgress() + 1 + "s");
                            mRecordingProgress.setProgress(mRecordingProgress.getProgress() + 1);
                        }
                    }

                    @Override
                    public void onFinish() {
                        if (mPlayer.isPlaying()) {
                            mPlayButton.setText("Play");
                            mPlayer.stop();
                            stopPlaying();
                            mPlayButton.mStartPlaying = !mPlayButton.mStartPlaying;
                            mRecordingProgress.setProgress(mRecordingProgress.getMax());
                            mRecordingProgressText.setText(mRecordingProgress.getMax() + "s");

                        }
                    }
                };
                mCountDownTimer.start();
            } else {
                mPlayer.setDataSource(mGuessProfile.getVoiceUrls().get(0));
                mPlayer.prepare();
                mPlayer.start();
                // count down timer in seconds
                mCountDownTimer = new CountDownTimer(30000, 500) {

                    @Override
                    public void onTick(long millisUntilFinished) {
                        // Nothing to do
                        if (!mPlayer.isPlaying()) {
                            mPlayButton.setText("Play");
                            mPlayer.stop();
                            stopPlaying();
                            mPlayButton.mStartPlaying = !mPlayButton.mStartPlaying;
                            mCountDownTimer.cancel();

                        }
                    }

                    @Override
                    public void onFinish() {
                        if (mPlayer.isPlaying()) {
                            mPlayButton.setText("Play");
                            mPlayer.stop();
                            stopPlaying();
                            mPlayButton.mStartPlaying = !mPlayButton.mStartPlaying;

                        }
                    }
                };
                mCountDownTimer.start();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void stopPlaying() {
        mPlayer.release();
        mPlayer = null;
    }
}
