package com.guess.pat.guess.services;

import com.guess.pat.guess.models.ResponseModel;
import com.guess.pat.guess.models.SendModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Paťo on 15.3.2018.
 */

public interface APIService {

    @Headers({
            "Content-Type:application/json",
            "Authorization:key=AAAAIsrgB8k:APA91bE7jLw273kVrOiHVgyP4fkeHpopEIze68m_piCqEcMJy0svTXPqrkpg6LtX_zFYwVMNTyzDHSF9a8jiTNDdr3klgVl8kZt7Mx-FO8E4xKWMO_oP7tqoG15DlxD19JHZ_ieeEMfc"
    })
    @POST("fcm/send")
    Call<ResponseModel> sendNotification (@Body SendModel body);
}
