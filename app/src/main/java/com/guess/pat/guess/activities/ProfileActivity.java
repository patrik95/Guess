package com.guess.pat.guess.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.guess.pat.guess.R;
import com.guess.pat.guess.models.ConfigurationModel;
import com.guess.pat.guess.models.DialectModel;
import com.guess.pat.guess.models.UserProfile;

import java.util.ArrayList;

/**
 * Created by Paťo on 7.10.2017.
 */

public class ProfileActivity extends AppCompatActivity {

    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;

    private String mUsername;
    private ConfigurationModel mConfigurationModel;

    private boolean mUserExistsInDatabase = false;

    // profile inputs
    private AutoCompleteTextView mAge;
    private AutoCompleteTextView mEducation;
    private AutoCompleteTextView mJob;
    private AutoCompleteTextView mCountry;
    private AutoCompleteTextView mDialect;

    private LinearLayout mDialectLayout;

    // firebase instances variables
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mProfileDatabaseReference;

    private Button mSaveButton = null;
    private Button mBackButton = null;
    private UserProfile mUserProfile;

    // progress bar layouts
    private RelativeLayout mProgressBarLayout;
    private RelativeLayout mProfileLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mUsername = getIntent().getStringExtra("USERNAME");

        // firebase
        mFirebaseDatabase = FirebaseDatabase.getInstance();

        mProfileDatabaseReference = mFirebaseDatabase.getReference().child("profiles");

        mProgressBarLayout = findViewById(R.id.id_progress_bar_layout);
        mProfileLayout = findViewById(R.id.id_activity_profile);

        // save profile button
        mSaveButton = findViewById(R.id.button_save);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // show progress bar and gray down background
                mProgressBarLayout.setVisibility(View.VISIBLE);
                mProfileLayout.setAlpha((float) 0.5);
                mProfileLayout.setClickable(false);

                // delete storage with voices and save new voices + update profile
                mProfileDatabaseReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (snapshot.exists()) {
                            mUserExistsInDatabase = true;
                            saveProfile();

                        } else {
                            mUserExistsInDatabase = false;
                            saveProfile();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
        });

        // get configuration data and set autocomplete view values
        getConfiguration ();
        setAutoCompleteViews();


        final Button button = findViewById(R.id.button_recordings);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserProfile profile = new UserProfile();
                profile.setUsername(mUsername);
                // open new activity (RecordingsActivity)
                Intent intent = new Intent(getBaseContext(), RecordingsActivity.class);
                intent.putExtra("PROFILE", profile);
                intent.putExtra("USERNAME", mUsername);
                startActivity(intent);
            }
        });

        mBackButton = this.findViewById(R.id.button_back );
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void saveProfile(){
        String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        // create new profile
        String token = FirebaseInstanceId.getInstance().getToken(); // TODO: there may be problem, probably it should be handled in FirebaseInstanceIDService

        mUserProfile = new UserProfile(token, mUsername, null, mAge.getText().toString(), mEducation.getText().toString(),
                mJob.getText().toString(), mCountry.getText().toString(), mDialect.getText().toString(),  null);

        if (mUserExistsInDatabase) {
            // edit existing profile
            mProfileDatabaseReference.child(userUid).child("token").setValue(token);
            mProfileDatabaseReference.child(userUid).child("age").setValue(mAge.getText().toString());
            mProfileDatabaseReference.child(userUid).child("education").setValue(mEducation.getText().toString());
            mProfileDatabaseReference.child(userUid).child("job").setValue(mJob.getText().toString());
            mProfileDatabaseReference.child(userUid).child("country").setValue(mCountry.getText().toString());
            mProfileDatabaseReference.child(userUid).child("dialect").setValue(mDialect.getText().toString());
        } else {
            mProfileDatabaseReference.child(userUid).setValue(mUserProfile);
        }
        storeUserData();
    }

    private void storeUserData() {
        // convert User object user to JSON format
        Gson gson = new Gson();
        String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();


        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.user_profile), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        String id = "profile_" + userUid; // get storage key

        // check if is mood set
        String user_json = sharedPref.getString(id,"");
        UserProfile profile  = gson.fromJson(user_json, UserProfile.class);
        if (!(profile.getMood() == null)) {
            mUserProfile.setMood(profile.getMood());
        }


        user_json = gson.toJson(mUserProfile);

        // store in SharedPreferences
        editor.putString(id, user_json);
        editor.commit();

        mProgressBarLayout.setVisibility(View.INVISIBLE);
        mProfileLayout.setClickable(true);
        mProfileLayout.setAlpha(1);
        finish();
    }

    private void getConfiguration () {
        Gson gson = new Gson();
        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.configuration), Context.MODE_PRIVATE);

        String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String id = "configuration_" + userUid; // get storage key

        String user_json = sharedPref.getString(id,null);
        mConfigurationModel  = gson.fromJson(user_json, ConfigurationModel.class);
    }

    // get data from default values or from database
    private void setAutoCompleteViews() {
        // user should not use application when internet is offline, but probably they should see some data anyway
        if (mConfigurationModel == null){
            // AGE
            mAge = findViewById(R.id.autoTextAge);
            // array adapter for autocomplete view
            ArrayAdapter<CharSequence> age_adapter = ArrayAdapter.createFromResource(this,
                    R.array.age, android.R.layout.simple_spinner_item);
            age_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            mAge.setAdapter(age_adapter);
            mAge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mAge.showDropDown();
                }
            });

            // EDUCATION
            mEducation = findViewById(R.id.spinnerEducation);
            // array adapter for autocomplete view
            ArrayAdapter<CharSequence> education_adapter = ArrayAdapter.createFromResource(this,
                    R.array.education, android.R.layout.simple_spinner_item);
            education_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mEducation.setAdapter(education_adapter);
            mEducation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mEducation.showDropDown();
                }
            });

            // JOBS
            mJob = findViewById(R.id.autoComplete_jobs);
            // array adapter for autocomplete view
            ArrayAdapter<CharSequence> jobs_adapter = ArrayAdapter.createFromResource(this,
                    R.array.countries, android.R.layout.simple_spinner_item);
            jobs_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mJob.setAdapter(jobs_adapter);

            mJob.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mJob.showDropDown();
                }
            });

            // COUNTRY NAMES
            mCountry = findViewById(R.id.spinnerCountry);
            // array adapter for autocomplete view
            ArrayAdapter<CharSequence> country_adapter = ArrayAdapter.createFromResource(this,
                    R.array.countries, android.R.layout.simple_spinner_item);
            country_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mCountry.setAdapter(country_adapter);

            mCountry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mCountry.showDropDown();
                }
            });

            // DIALECT
            mDialect = findViewById(R.id.autoComplete_dialect);
            // array adapter for autocomplete view
            ArrayAdapter<CharSequence> dialect_adapter = ArrayAdapter.createFromResource(this,
                    R.array.dialect, android.R.layout.simple_spinner_item);
            dialect_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mDialect.setAdapter(dialect_adapter);

            mDialect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mDialect.showDropDown();
                }
            });

            loadStoredDataFromDefault(age_adapter, education_adapter, jobs_adapter, country_adapter, dialect_adapter);
        } else {
            // AGE
            mAge = findViewById(R.id.autoTextAge);
            // array adapter for autocomplete view
            ArrayAdapter<String> age_adapter = new ArrayAdapter<String>(this,
                    R.layout.custom_dropdown, mConfigurationModel.getAges());
            mAge.setAdapter(age_adapter);
            mAge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mAge.showDropDown();
                }
            });

            // EDUCATION
            mEducation = findViewById(R.id.spinnerEducation);
            // array adapter for autocomplete view
            ArrayAdapter<String> education_adapter = new ArrayAdapter<String>(this,
                    R.layout.custom_dropdown, mConfigurationModel.getEducation());
            mEducation.setAdapter(education_adapter);
            mEducation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mEducation.showDropDown();
                }
            });

            // JOBS
            mJob = findViewById(R.id.autoComplete_jobs);
            // array adapter for autocomplete view
            ArrayAdapter<String> jobs_adapter = new ArrayAdapter<String>(this,
                    R.layout.item_dropdown, R.id.item, mConfigurationModel.getJobs());
            mJob.setAdapter(jobs_adapter);

            mJob.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mJob.showDropDown();
                }
            });

            //COUNTRY NAMES
            mCountry = findViewById(R.id.spinnerCountry);
            // array adapter for autocomplete view
            ArrayAdapter<String> country_adapter = new ArrayAdapter<String>(this,
                    R.layout.item_dropdown, R.id.item, mConfigurationModel.getCountries());
            mCountry.setAdapter(country_adapter);

            mCountry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mCountry.showDropDown();
                }
            });
            mCountry.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String country = mCountry.getText().toString();
                    ArrayList<DialectModel> dialects = mConfigurationModel.getDialects();
                    ArrayAdapter<String> dialect_adapter;
                    if (country.equals("Slovakia")){
                        dialect_adapter = new ArrayAdapter<String>(ProfileActivity.this,
                                R.layout.custom_dropdown, dialects.get(2).getValues());
                        mDialectLayout.setVisibility(View.VISIBLE);
                    } else if (country.equals("Czech Republic")){
                        dialect_adapter = new ArrayAdapter<String>(ProfileActivity.this,
                                R.layout.custom_dropdown, dialects.get(1).getValues());
                        mDialectLayout.setVisibility(View.VISIBLE);
                    } else {
                        dialect_adapter = new ArrayAdapter<String>(ProfileActivity.this,
                                R.layout.custom_dropdown, dialects.get(0).getValues());
                        mDialectLayout.setVisibility(View.GONE);
                    }

                    mDialect.setAdapter(dialect_adapter);
                }
            });

            // DIALECT
            mDialectLayout = findViewById(R.id.id_dialect_layout);
            mDialect = findViewById(R.id.autoComplete_dialect);
            // array adapter for autocomplete view
            ArrayAdapter<String> dialect_adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, mConfigurationModel.getDialects().get(1).getValues());
            dialect_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mDialect.setAdapter(dialect_adapter);

            mDialect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    mDialect.showDropDown();
                }
            });

            loadStoredData(age_adapter, education_adapter, jobs_adapter, country_adapter, dialect_adapter);
        }
    }

    // set data to autocomplete view
    private void loadStoredDataFromDefault(ArrayAdapter<CharSequence> age_adapter, ArrayAdapter<CharSequence> education_adapter,
                                           ArrayAdapter<CharSequence> jobs_adapter,  ArrayAdapter<CharSequence> country_adapter,  ArrayAdapter<CharSequence> dialect_adapter) {

        Gson gson = new Gson();
        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.user_profile), Context.MODE_PRIVATE);

        String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String id = "profile_" + userUid; // get storage key

        String user_json = sharedPref.getString(id,"");
        mUserProfile  = gson.fromJson(user_json, UserProfile.class);

        if (!(mUserProfile == null)) {
            mAge.setListSelection(age_adapter.getPosition(mUserProfile.getAge()));
            mEducation.setListSelection(education_adapter.getPosition(mUserProfile.getEducation()));
            mJob.setListSelection(jobs_adapter.getPosition(mUserProfile.getJob()));
            mCountry.setListSelection(country_adapter.getPosition(mUserProfile.getCountry()));
            mDialect.setListSelection(dialect_adapter.getPosition(mUserProfile.getDialect()));
        }

    }

    // set data to autocomplete view
    private void loadStoredData(ArrayAdapter<String> age_adapter, ArrayAdapter<String> education_adapter,
                                ArrayAdapter<String> jobs_adapter, ArrayAdapter<String> country_adapter, ArrayAdapter<String> dialect_adapter) {

        Gson gson = new Gson();
        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.user_profile), Context.MODE_PRIVATE);

        String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String id = "profile_" + userUid; // get storage key

        String user_json = sharedPref.getString(id,"");
        mUserProfile  = gson.fromJson(user_json, UserProfile.class);

        if (!(mUserProfile == null)) {
            mAge.setListSelection(age_adapter.getPosition(mUserProfile.getAge()));
            mAge.setText(mUserProfile.getAge());
            mEducation.setListSelection(education_adapter.getPosition(mUserProfile.getEducation()));
            mEducation.setText(mUserProfile.getEducation());
            mJob.setListSelection(jobs_adapter.getPosition(mUserProfile.getJob()));
            mJob.setText(mUserProfile.getJob());
            mCountry.setListSelection(country_adapter.getPosition(mUserProfile.getCountry()));
            mCountry.setText(mUserProfile.getCountry());
            mDialect.setListSelection(dialect_adapter.getPosition(mUserProfile.getDialect()));
            mDialect.setText(mUserProfile.getDialect());
        }

    }
}
