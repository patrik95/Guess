package com.guess.pat.guess.models;


import java.io.Serializable;

/**
 * Created by Paťo on 23.2.2018.
 */

public class PartResult implements Serializable {

    private String name;
    private String answer;
    private boolean result;

    public PartResult() {
    }

    public PartResult(String name, String answer, boolean result) {
        this.name = name;
        this.answer = answer;
        this.result = result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean getPartResult() {
        return result;
    }

    public void setPartResult(boolean result) {
        this.result = result;
    }
}
