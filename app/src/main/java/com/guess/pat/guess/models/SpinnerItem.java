package com.guess.pat.guess.models;

import android.net.Uri;

import com.google.firebase.storage.StorageReference;

import java.io.Serializable;

/**
 * Created by Paťo on 12.2.2018.
 */

public class SpinnerItem implements Serializable {

    private int orderNumber;
    private String name;
    private String filePath;
    private StorageReference storageReference;

    public SpinnerItem(int orderNumber, String name, String filePath) {
        this.orderNumber = orderNumber;
        this.name = name;
        this.filePath = filePath;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public StorageReference getStorageReference() {
        return storageReference;
    }

    public void setStorageReference(StorageReference storageReference) {
        this.storageReference = storageReference;
    }
}
